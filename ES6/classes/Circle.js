class Circle extends Shape {
    constructor (id, x, y, radius) {
        super(id, x, y);
        this.radius = radius;
    }
    toStringValue () {
        return "Circle > " + super.toStringValue();
    }
    static defaultCircle() {
        return new Circle("default", 0, 0, 100);
    }
}