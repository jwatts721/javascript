class Colored {
    initializer ()     { this._color = "white"; }
    get color ()       { return this._color; }
    set color (v)      { this._color = v; }
}