class Rectangle extends Shape {
    constructor (id, x, y, width, height) {
        super(id, x, y);
        this._width  = width;
        this._height = height;
    }
    toStringValue () {
        return "Rectangle > " + super.toStringValue();
    }
    static defaultRectangle () {
        return new Rectangle("default", 0, 0, 100, 100);
    }
    set width  (width)  { this._width = width;               }
    get width  ()       { return this._width;                }
    set height (height) { this._height = height;             }
    get height ()       { return this._height;               }
    get area   ()       { return this._width * this._height; }
}