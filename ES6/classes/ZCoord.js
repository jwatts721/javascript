class ZCoord {
    initializer ()     { this._z = 0; }
    get z ()           { return this._z; }
    set z (v)          { this._z = v; }
}