const fetch = require('node-fetch');

async function fetchData()
{
    let response = await fetch('http://localhost:3000/api');
    console.log(response.status);
    console.log(response.statusText);
    
    if (response.status === 200) {
        let json = await response.json();
        console.log(json);
    }
}

fetchData();