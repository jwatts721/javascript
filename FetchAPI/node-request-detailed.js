const { default: fetch } = require("node-fetch");

//const fetch = request('node-fetch');

fetch('http://localhost:3000/api', {
    method: 'GET',
    // body: 'a=1' <-- would be for post data
})
    .then(res => res.json())
    .then(json => {
        console.log(json);
    })
    .catch(err => console.log(err.message));
