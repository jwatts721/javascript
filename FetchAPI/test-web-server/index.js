var express = require('express');
var fs = require('fs');
var app = express();

app
    .get('/api', function(request,response){
        var data = 
            [
                {id: 1, first_name: 'John', last_name: 'Doe'},
                {id: 2, first_name: 'Jane', last_name: 'Doe'},
                {id: 3, first_name: 'Jim', last_name: 'Beean'},
            ];
        response.type('json');
        response.send(data);
    })
    .get('/', function(request, response){
        fs.readFile('index.html', 'utf8', (err, html) => {
            if (err) {
                response.send('<h1>' + err.message + '</h1>');
                return;
            }
            response.send(html);
        });
    })
    .listen(3000);