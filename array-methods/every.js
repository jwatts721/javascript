const items = require('./items-data');

const allUnder100 = items.every((item) => {
    return item.price <= 100;
});

console.log(items);
console.log('Are all items under 100? ' + allUnder100);