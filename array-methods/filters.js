const items = require('./items-data');

const filteredItems = items.filter((item) => {
    return item.price <= 100;
});

console.log(filteredItems);