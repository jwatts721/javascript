const items =
    [
        { name: 'Bike', price: 100 },
        { name: 'TV', price: 200 },
        { name: 'Album', price: 20 },
        { name: 'Book', price: 5 },
        { name: 'Phone', price: 500 },
        { name: 'Computer', price: 1000 },
        { name: 'Keyboard', price: 25 }
    ];

module.exports = items;