const items = require('./items-data');

const itemNames = items.map((item) => {
    return item.name;
});

const itemPrices = items.map((item) => {
    return item.price;
})

console.log(itemNames);
console.log(itemPrices);