const items = require('./items-data');

const total = items.reduce((currentTotal, item) => {
    return item.price + currentTotal
}, 0);

console.log(items);
console.log("Total: " + total);