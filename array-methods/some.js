const items = require('./items-data');

const hasInexpensiveItems = items.some((item) => {
    return item.price <= 100;
});

console.log(items);
console.log('Are there inexpensive items: ' + hasInexpensiveItems);