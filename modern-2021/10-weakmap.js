/** 
 * Explanation 
 * The JavaScript engine keeps a value in memory while it is "reachable" and can be potentially used. 
 * If the value is not reachable, it is garbage collected.
 */
let john = { name: "John" };
let array = [ john ];
john = null; // overwrite the reference, will be garbage collected

console.log(array[0]); // john
console.log(john); // null

// The object previously referenced by john is stored inside the array
// therefore it won't be garbage collected
// We can get it as array[0]

// Also with Map, while Map exists, the object exists and cannot be garbage collected
let jake = { name: "Jake" };
let map = new Map();
map.set(jake, "Hey Jake");
jake = null; // overwrite the reference

// jake is stored inside the map, cannot be garbage collected
console.log(map);
console.log(jake); // null

/************************
 * WEAK MAP - does NOT prevent garbage collection of key objects
 ***********************/
// Rule 1 - Weak Map keys must be objects, not primitives
let weakMap = new WeakMap();
let obj = {};

weakMap.set(obj, "ok"); // works fine because obj is an object

try {
    weakMap.set('test', 'Whoops'); // Error, because 'test' is not an object
    weakMap.set(1, 'one'); // Error, because 1 is a primitive type
} catch (e) {
    console.log(e.toString());
}

// If we use an object as a key adn the reference to the object is removed, the WeakMap will also be removed from memory
let jack = { name: "Jack" };
let wm = new WeakMap();
wm.set(jack, 'Hey Jack'); 
console.log(wm);

jack = null; // overwrite the reference

// jack will also be removed from memory!
console.log(jack);
console.log(wm);

/**************
 * WeakMap Use Case 1 - Additional Storage Example
 * - Storing data that only exists when the object is alive
 */
let joe = { name: "Joe" }

let joeMap = new WeakMap();
joeMap.set(joe, 'secret documents');
// if joe dies, secret documents will be destroyed automatically 

// If we were to use a Map, with a counter, we can destroy "joe", but his counter record would still be linked to the map
// WeakMap benefits:
let visitsCountMap = new WeakMap(); // weakmap: user => visits count
visitsCountMap.set(joe, 0);

// increase the visit count
function countUser(user) {
    let count = visitsCountMap.get(user) || 0;
    visitsCountMap.set(user, count + 1);
}
console.log('The map data');
console.log(visitsCountMap.get(joe));
console.log('Execute the count');
countUser(joe);
console.log('After the counter');
console.log(visitsCountMap.get(joe));


console.log('Destroy the "joe" record'); 
joe = null; // overwrite the reference
console.log('The weakMap should also be destroyed');


/**************
 * Store read dates
 * The question now is: which data structure you’d suggest to store the information: “when the message was read?”.

    In the previous task we only needed to store the “yes/no” fact. Now we need to store the date, and it should only remain in memory until the message is garbage collected.

    P.S. Dates can be stored as objects of built-in Date class, that we’ll cover later.
 */
let readMap = new WeakMap();
readMap.set(messages[0], new Date(2017, 1, 1));