/**********
 * Just like a WeakMap, we can only add objects as keys (not primitives) for a WeakSet
 */

// Being "weak", it can serve as additional storage. But not for arbitrary data, rather for "yes/no" facts. A membership in WeakSet
// may mean something about the object

// For instance, we can add users to WeakSet to keep track of those who visited our site. 
let visitedSet = new WeakSet();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

visitedSet.add(john); // John visited us
visitedSet.add(pete); // Then Pete
visitedSet.add(john); // John again

// visitedSet has 2 users now

// check if John visited?
console.log(visitedSet.has(john)); // true

// check if Mary visited?
console.log(visitedSet.has(mary)); // false

john = null;

// visitedSet will be cleaned automatically

console.log(visitedSet);

visitedSet.delete(john); 
console.log(visitedSet.has(john));

/**
 * The most notable limitation of WeakMap and WeakSet is the absence of iterations, and the inability to get all current content. 
 * That isn't the main job which is to be "additional"sotrage for data for objcts that are stored/managed in another place.
 */

/** 
 * Exercise 1 - Store "unread" flags
 * - Your code can access it, but the messages are managed by someone else's code. New messages are added, old ones are removed regularly by that code, 
 *   and you don't know the exact moments when it happens
 * 
 *  Now, which data structure could you use to store information about whether the message “has been read”? The structure must be well-suited to give the answer “was it read?” for the given message object.

    P.S. When a message is removed from messages, it should disappear from your structure as well.

    P.P.S. We shouldn’t modify message objects, add our properties to them. As they are managed by someone else’s code, that may lead to bad consequences.
*/
let messages = [
    {text: "Hello", from: "John"},
    {text: "How goes?", from: "John"},
    {text: "See you soon", from: "Alice"}
  ];
console.log(messages);

let readMessages = new WeakMap();
messages.forEach(item => {
    readMessages.set(item);    
})
// readMessages now has 3 unique elements
readMessages.set(messages[1]);
    // readMessages still has 3 unique elements

// Answer: Was the first message read? 
console.log("Read message 0: " + readMessages.has(messages[0]));
console.log(messages[0]);
console.log('\n');

messages.shift();
    // now readMessages has 2 unique elemtns (technically memory may be cleaned later)

console.log("Read message 0: " + readMessages.has(messages[1]));
console.log(messages[1]);
console.log('\n');


console.log("Read message 0: " + readMessages.has(messages[2]));
console.log(messages[2]);
console.log('\n');


