/**
 * Array destructuring
 */
let arr = ["John", "Smith"];

// DESTRUCTURING THE ARRAY
// - opposed to let firstName = arr[0]
let [firstName, lastName] = arr;
// Now we can work with variables instead of array members
console.log(`First Name: ${firstName}`);
console.log(`Last Name: ${lastName}`)

// Also can work with "split" method
let [firstName2, lastName2] = "Joe Wallace".split(' ');
console.log('First Name: ' + firstName2);
console.log('Last Name: ' + lastName2);


// IGNORING ELEMENTS
// - Elements can be thrown away using commas
let [firstName3, , title] = ["Julius", "Caesar", "Consul"];
console.log(title); // Consul

// WORKS WITH ANY ITERABLES, NOT JUST ARRAYS
let [a, b, c] = "abc"; // ["a", "b", "c"]
console.log(`${a} ${b} ${c}`);
let [one, two, three] = new Set([1, 2, 3]);

// CAN ASSIGN TO ANYTHING ON THE LEFT SIDE
let user = {};
[user.name, user.surname] = "Jimmy Smith".split(' ');
console.log(user.name);
console.log(user.surname);

// LOOPING WITH .entries()
let user2 = {
    name: "John",
    age: 30
}
for (let [key, value] of Object.entries(user2)) {
    console.log(`${key} ${value}`);
}
    
    // Map has built in iteration 
let user3 = new Map();
user3.set('name', 'Jack');
user3.set('age', 40);
for (let [key, value] of user3) {
    console.log(`${key} ${value}`);
}

// Swap Variables Trick
let guest = "Jane";
let admin = "Pete";

// Let's swap the values: guest=Peta, admin=Jane
[guest, admin] = [admin, guest];
console.log(`Guest = ${guest} - Admin = ${admin}`);

// The Rest `...`
    // Usually if the array is longer than the list at the left, the "extra" items are omitted.
let [name1, name2] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];

console.log(name1); // Julius
console.log(name2); // Caesar
// Further items aren't assigned anywhere

let [first, second, ...rest] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];

// rest is array of items, starting from the 3rd one
console.log(rest[0]); // Consul
console.log(rest[1]); // of the Roman Republic
console.log(rest.length); // 2

// We can use other variable names instead of `rest`
let [first_name, second_name, ...titles] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];
// now titles = ["Consul", "of the Roman Republic"]
console.log(titles);

// Default Values
let [name = "Guest", surname = "Anonymouse"] = ["Julius"];
console.log(name);    // Julius (from array)
console.log(surname); // Anonymouse (default used)

/** 
 * -- Browser Code Only 
// runs only prompt for surname
let [name = prompt('name?'), surname = prompt('surname?')] = ["Julius"];

alert(name);    // Julius (from array)
alert(surname); // whatever prompt gets
 */

// Object destructuring
let options = {
    title: "Menu",
    width: 100,
    height: 200
  };
  // Here we can rename a value - assigning to local variable name theTitle
    // this may be a good practice as the names on the left only match to the position in the object
  let {title: theTitle, width, height} = options;
  
  console.log(theTitle);  // Menu
  console.log(width);  // 100
  console.log(height); // 200

  let {width: w, height: h, title: oTitle} = options;
  console.log(w);
  console.log(h);
  console.log(oTitle);

// We can assign default values with "=" when valurs are missing
let options2 = {
    title: "The Title"
}
let {width2 = 100, height2=200, title: otitle} = options2;
console.log(otitle);
console.log(width2);
console.log(height2);

// We can combine both colon and equal
let options3 = {
    title3: "Menu"
};

let {width3: w3 = 100, height3: h3 = 200, title3} = options3;
  
console.log(title3);  // Menu
console.log(w3);      // 100
console.log(h3);      // 200

// The rest pattern `...` on the object
let options4 = {
    title4: "Menu",
    height4: 200, 
    width4: 100
}

let {title4, ...rest4} = options4;
console.log(rest4.height4);
console.log(rest4.width4);

//  Nested destructuring
options = {
    size: {
      width: 100,
      height: 200
    },
    items: ["Cake", "Donut"],
    extra: true
  };
  
  // destructuring assignment split in multiple lines for clarity
  let {
    size: { // put size here
      width: width5,
      height: height5
    },
    items: [item1, item2], // assign items here
    title5 = "Menu" // not present in the object (default value is used)
  } = options;
  
  console.log(title5);  // Menu
  console.log(width5);  // 100
  console.log(height5); // 200
  console.log(item1);  // Cake
  console.log(item2);  // Donut

  // Smart Functions
    // BAD WAY to write a function
function badFunction(title = "Untitled", width = 200, height = 100, items = []) {}

  /**
   * The Problem: 
   *    1 - How do you remember the order of arguments.
   *    2 - How do you call a function when most parameters are ok by default
   */
badFunction("My Function Call", undefined, undefined, ['Item1', 'Item2']) // <-- THIS IS JUST PLAIN UGLY AND UNREADABLE

    // THE BETTER WAY
// we pass object to function
options = {
    title: "My Menu",
    items: ["Item1", "Item2"]
}

// ... and it immediately expands into variables
function showMenu({title = "Untitled", width = 200, height = 100, items = []}) {
    // title, items – taken from options,
    // width, height – defaults used
    console.log( `${title} ${width} ${height}` ); // My Menu 200 100
    console.log( items ); // Item1, Item2
}
showMenu(options);

// More Complex destructuring
options = {
    title: "My menu",
    items: ["Item1", "Item2"]
};

function showMenu({
    title = "Untitled",
    width: w = 100,  // width goes to w
    height: h = 200, // height goes to h
    items: [item1, item2] // items first element goes to item1, second to item2
}) {
    console.log( `${title} ${w} ${h}` ); // My Menu 100 200
    console.log( item1 ); // Item1
    console.log( item2 ); // Item2
}
showMenu(options);


// Supporting defaults
function showMenu({
    title = "Untitled",
    width = 100,
    height = 200
}) {
    console.log( `${title} ${width} ${height}` ); // My Menu 100 200
}

showMenu({}); // ok, all values are default - error if not assigned default object

// showMenu(); // Error
     // We can fix this by setting the default object to the entire parameter
function showMenu({
    title = "Untitled",
    width = 100,
    height = 200
} = {}) {
    console.log( `${title} ${width} ${height}` ); // My Menu 100 200
}
showMenu();    

// Exercise
let johnRecord = { name: "John", years: 30 };
/**
 * Write the destructuring assignment that reads:
    name property into the variable name.
    years property into the variable age.
    isAdmin property into the variable isAdmin (false, if no such property)
 */
let {name: userName, years: age, isAdministrator = false} = johnRecord;
console.log(userName);
console.log(age);
console.log(isAdministrator);


let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250
};
function getTopPaid(records = {}) {
    let topPaidName = null;
    let maxSalary = 0;


    if (Object.keys(records).length === 0) return null;

    for (let [name, salary] of Object.entries(records)) {
        if (salary > maxSalary) {
            maxSalary = salary;
            topPaidName = name;
        }
    }
    return topPaidName;
}

console.log(getTopPaid());
console.log(getTopPaid(salaries));