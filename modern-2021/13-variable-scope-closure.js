// Code Blocks with isolated code
{
    let message = "Hello";
    console.log(message); // Hello
}
//console.log(message); <--- produces error

/*
All produce error without quotes
// show message
let message = "Hello";
alert(message);

// show another message
let message = "Goodbye"; // Error: variable already declared
alert(message);
*/
{
    // show message
    let message = "Hello";
    console.log(message);
}

{
    // show another message
    let message = "Goodbye";
    console.log(message);
}

// Nested Functions
function sayHiBye(firstName, lastName) {
    function getFullName() {
        return `${firstName} ${lastName}`;
    }

    console.log("Hello, " + getFullName());
    console.log("Bye, " + getFullName());
}
sayHiBye("John", "Doe");

// Clousure
// A closure is a function that remembers its outer variables and can access them. 
// Solution
// Write a sum with closures
function sum(a) {
    console.log(`Outer A is ${a}`);
    return function (b) {
        console.log(`Inner A is ${a}`);
        console.log(`Inner B is ${b}`);
        return a + b; // takes "a" from the our lexical environment
    }
}
console.log(sum(1)(2)); // 3
console.log(sum(5)(-1)); // 4

// Filter through function
let arr = [1, 2, 3, 4, 5, 6, 7];
console.log(arr.filter(inBetween(3, 6))); // 3, 4, 5, 6
console.log(arr.filter(inArray([1, 2, 10]))); // 1, 2

function inBetween(min, max) {
    return function (x) {   // <-- can also use short syntax for function -> (x) => x >= min...
        return x >= min && x <= max;
    }
}

function inArray(values) {
    return (a) => values.includes(a);
}

// Sort by field
let users = [
    { name: "John", age: 20, surname: "Johnson" },
    { name: "Pete", age: 18, surname: "Peterson" },
    { name: "Ann", age: 19, surname: "Hathaway" }
];
console.log(users);

function byField(field) {
    return (a, b) => a[field] > b[field] ? 1 : -1;
}

users.sort(byField('name'));
console.log(users);
users.sort(byField('age'));
console.log(users);

// Army of Functions
{
    function makeArmy() {
        let shooters = [];

        let i = 0;
        while (i < 10) {
            let shooter = function () { // create a shooter function,
                console.log(i); // that should show its number
            };
            shooters.push(shooter); // and add it to the array
            i++;
        }

        // ...and return the array of shooters
        return shooters;
    }

    let army = makeArmy();

    // all shooters show 10 instead of their numbers 0, 1, 2, 3...
    army[0](); // 10 from the shooter number 0
    army[1](); // 10 from the shooter number 1
    army[2](); // 10 ...and so on.
}

// Solution to Army of functions
{
    function makeArmy() {
        let shooters = [];

        let i = 0;
        while (i < 10) {
            let value = i;
            let shooter = function () { // create a shooter function,
                console.log(value); // that should show its number
            };
            shooters.push(shooter); // and add it to the array
            i++;
        }

        // ...and return the array of shooters
        return shooters;
    }

    let army = makeArmy();

    // all shooters show 10 instead of their numbers 0, 1, 2, 3...
    army[0](); // 10 from the shooter number 0
    army[1](); // 10 from the shooter number 1
    army[2](); // 10 ...and so on.
}