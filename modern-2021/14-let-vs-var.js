// "var" has no block scope
{
    if (true) {
        let test = true;
    }
    console.log(test); // undefined - does not live after if
    
}
{
    if (true) {
        var test = true;
    }
    console.log(test); // true, variable lives after if
}

// "var" tolerates redeclarations
{
    let user; 
    // let user; // SyntaxError
}

{ 
    var user = "Pete";
    console.log(user);

    var user = "John";
    console.log(user);
}