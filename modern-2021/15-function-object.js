/**
 * The "name" property
 */
{
    function sayHi() {
        console.log('Hi there...');
    }
    console.log(sayHi.name);

    let sayHello = function () { console.log('Hello...'); }
    console.log(sayHello.name);

    // Also works in objects
    let user = {
        sayHi: function () { console.log('Hi!'); }
    }
    console.log(user.sayHi.name);
}

/**
 * 
 * The "length" property 
 */
function lengthOfFunction(a) { console.log("Length of this function"); }
console.log(lengthOfFunction.length);
function lengthOfFunctionParams(a, b, c, ...rest) { console.log("Length of the params - rest not counted"); }
console.log(lengthOfFunctionParams.length);


function ask(questionAndAnswer, ...handlers) {
    let [question, answer] = questionAndAnswer;
    let isYes = answer === 'Y';
    console.log(question);
    for (let handler of handlers) {
        if (handler.length === 0) { // as in the length of te parameters of the function
            if (isYes) handler();
        } else {
            handler(isYes);
        }
    }
}

// for positive answer, both handlers are called
// for negative answer, only the second one
ask(["Question?", "Y"], () => console.log('You said yes'), result => console.log(result));
ask(["Question?", "N"], () => console.log('You said yes'), result => console.log(result));


/**
 * Custom properties
 * - A property is not a variable
 */
{
    function sayHi() {
        console.log("Hi");

        // let's count how many times we run
        sayHi.counter++;
    }
    sayHi.counter = 0; // initial value

    sayHi(); // Hi
    sayHi(); // Hi

    console.log(`Called ${sayHi.counter} times`); // Called 2 times
}

/**
 * Named Function Expression
 */
{
    let sayHi = function (who) {
        console.log(`Hello, ${who}`);
    };
    sayHi("John");
}
{
    // Defined here as a function expression
    let sayHi = function func(who) {
        console.log(`Hello, ${who}`);
    };
    sayHi("John"); // Nothing special here
}
// But we can do this 
{
    let sayHi = function func(who) {
        if (who) {
            console.log(`Hello, ${who}`);
        } else {
            func("Guest"); // use func to re-call itself
            // why use this, because sayHi call to itself won't recognize itself as a function
            // sayHi("Guest") <-- would produce an error
        }
    };
    sayHi();
}
// func() <-- this won't work as it is not visible outside of the functions