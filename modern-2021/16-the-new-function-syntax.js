/**
 * The "new Function" syntax
 */
let sum = new Function('a', 'b', 'return a + b');
console.log(sum(1, 2));

let sayHi = new Function('console.log("Hi");');
sayHi();

// This is ideal when you don't have the script defined ahead of time

// Closure
{
    function getFunc() {
        let value = "Test";
        let func = new Function(`console.log(${value})`); // <-- as you can see, it is not always useful
        return func;
    }
    //getFunc()(); <-- would produce an error
}
{
    function getFunc() {
        let value = "Test";
        let func = function () { console.log(value); }
        return func;
    }
    getFunc()();
}
