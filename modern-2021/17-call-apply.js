/**
 * Using "func.call"
 */
{
    function sayHi() {
        console.log(this.name);
    }
    let user = { name: "John" };
    let admin = { name: "Admin" };

    // use call to pass different objects as "this"
    sayHi.call(user); // John
    sayHi.call(admin); // Admin
}
{
    // Can use with parameters
    function say(phrase) {
        console.log(this.name + ': ' + phrase);
    }

    let user = { name: "John" };

    // user becomes this, and "Hello" becomes the first argument
    say.call(user, "Hello"); // John: Hello
}

/**
 * Using "func.apply"
 */
{
    function sayHi() {
        console.log(this.name)
    }
    let user = { name: "John" };
    let admin = { name: "Admin" };
    sayHi.apply(user); // <-- similar to .call
}
{
    function say(phrase) {
        console.log(this.name + ': ' + phrase);
    }

    let user = { name: "John" };

    // user becomes this, and "Hello" becomes the first argument
    say.apply(user, ["Hello"]); // John: Hello <-- arguments need to be passed as array
}