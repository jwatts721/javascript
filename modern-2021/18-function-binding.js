/**
 * Losing "this"
 */
{
    let user = {
        firstName: "John",
        sayHi() {
            console.log(`Hello, ${this.firstName}!`);
        }
    };

    setTimeout(user.sayHi, 1000); // Hello, undefined! <-- we just lost the reference
    let f = user.sayHi;
    setTimeout(f, 1000); // lost user context
}
/** Solution - a wrapper*/
{
    let user = {
        firstName: "John",
        sayHi() {
            console.log(`Hello, ${this.firstName}!`);
        }
    };

    setTimeout(function () {
        user.sayHi(); // Hello, John! <-- works because it receives user from the outer lexical environment
    }, 1000);

    //setTimeout(() => user.sayHi(), 1000); // <-- shorthand
}
/** Vulnerable to changes before the timeout */
{
    let user = {
        firstName: "John",
        sayHi() {
            console.log(`Hello, ${this.firstName}!`);
        }
    };
    setTimeout(() => user.sayHi(), 1000);

    // Vulnerable to changes
    // Here we immediately change the value of user, but the execution happens in 1 second
    user = {
        sayHi() { console.log("Another user in setTimeout!"); }
    }
}
/** Solution - Function Binding */
{
    let user = {
        firstName: "Jack"
    };

    function func() {
        console.log(this.firstName);
    }

    let funcUser = func.bind(user);
    funcUser(); // Jack
}
// Now let's fix our timeout function
{
    let user = {
        firstName: "Jack",
        sayHi() {
            console.log(`Hello, ${this.firstName}!`);
        }
    };
    let sayHi = user.sayHi.bind(user);
    setTimeout(sayHi, 1000);

    // even if the value of user changes within 1 second
    // sayHi uses the pre-bound value which is reference to the old user object
    user = {
        sayHi() { console.log("Another Jack in setTimeout!"); }
    }
}

// We can bind all objects in an iteration for convenience if we are passing the object around

/**
 * Binding arguments to Partial Functions
 */
function multiply(a, b) {
    return a * b;
}
let double = multiply.bind(null, 2);
console.log(double(3)); // = multiply(2, 3) = 6
//The call to mul.bind(null, 2) creates a new function double that passes calls to mul, fixing null as the context and 2 as the first argument. 
// Further arguments are passed “as is”.
let triple = multiply.bind(null, 3);
console.log(triple(3)); // = multiply (3, 3) = 9

