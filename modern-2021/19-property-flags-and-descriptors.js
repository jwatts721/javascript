/**
 * Property Flags
 * Object properties, besides a value, have three special attributes (so-called “flags”):
        writable – if true, the value can be changed, otherwise it’s read-only.
        enumerable – if true, then listed in loops, otherwise not listed.
        configurable – if true, the property can be deleted and these attributes can be modified, otherwise not.
 */
{
    let user = {
        name: "John"
    };

    let descriptor = Object.getOwnPropertyDescriptor(user, 'name');

    console.log(JSON.stringify(descriptor, null, 2));
}
// Define a property - by default they will all be false
{
    let user = {};
    Object.defineProperty(user, 'name', { value: 'John' });
    let descriptor = Object.getOwnPropertyDescriptor(user, 'name');
    console.log(JSON.stringify(descriptor, null, 2));
}
// Define a property with custom settings - Not Writable
{
    let user = { name: "John" };
    Object.defineProperty(user, 'name', { writable: false });
    let descriptor = Object.getOwnPropertyDescriptor(user, 'name');
    console.log(JSON.stringify(descriptor, null, 2));
    user.name = "Pete"; // Error: Cannot assingn to read only property 'name' <-- error only appears in 'use strict'; mode
    console.log(user.name); // Still "John"
}

// Not Enumerable
{
    let user = {
        name: "John",
        toString() {
            return this.name;
        }
    };

    // By default, both our properties are listed:
    for (let key in user) console.log(key); // name, toString
}
{
    let user = {
        name: "John",
        toString() {
            return this.name;
        }
    };

    Object.defineProperty(user, "toString", {
        enumerable: false
    });

    // Now our toString disappears:
    for (let key in user) console.log(key); // name
}

// Non-configurable
{
    let descriptor = Object.getOwnPropertyDescriptor(Math, 'PI');

    console.log(JSON.stringify(descriptor, null, 2));
    Math.PI = 3; // Error, because it has writable: false
    // delete Math.PI won't work either

    // Error, because of configurable: false
    Object.defineProperty(Math, "PI", { writable: true });
}

/**
 * Can also define multiple properties at once.
 * Syntax:
    Object.defineProperties(obj, {
        prop1: descriptor1,
        prop2: descriptor2
        // ...
    });
 */

// Sealing an object globally
/**
Object.preventExtensions(obj)
    Forbids the addition of new properties to the object.


Object.seal(obj)
    Forbids adding/removing of properties. Sets configurable: false for all existing properties.


Object.freeze(obj)
    Forbids adding/removing/changing of properties. Sets configurable: false, writable: false for all existing properties.
    And also there are tests for them:


Object.isExtensible(obj)
    Returns false if adding properties is forbidden, otherwise true.


Object.isSealed(obj)
    Returns true if adding/removing properties is forbidden, and all existing properties have configurable: false.


Object.isFrozen(obj)
    Returns true if adding/removing/changing properties is forbidden, and all current properties are configurable: false, writable: false.
    These methos are rarely used in practice.
    
 */