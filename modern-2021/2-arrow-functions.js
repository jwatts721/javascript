// Two parameters
let func = (param1, param2) => console.log("Here are the 2 pameters " + param1 + " - " + param2);

func(45, 57);

// One parameter
let n = n => n * 10;
let value = n(3);

console.log('Value of n: ' + value);

// No Parameters
let sayHi = () => console.log('Hello');
sayHi();

let age = 18;
let welcome = (age < 21) ?
    () => console.log('Show ID') :
    () => console.log('Come right in');
welcome();


// Multiline arrow functions
let sum = (a, b) => {
    let result = a + b;
    return result;
}
let value2 = sum(10, 23);
console.log('Value is: ' + value2);

// Has no "this"
let group = {
    title: "Our Group",
    students: ["John", "Pete", "Alice"],

    showList() {
        this.students.forEach(
            student => console.log(this.title + ': ' + student) // <-- this is referenced from the outer scope
        );
        /**
         * This code would have produced an errorCode
         * this.students.forEach(function(student) {
                // Error: Cannot read property 'title' of undefined
                alert(this.title + ': ' + student);
           });
         */
    }
};

group.showList();

/**
 * Other Arrow Function Rules
* - Does not have a "this" reference 
* - can't run with new - cannot be used with constructors
* - Have no "arguments" - there is no "arguments" variable to access with arrow functions
* - Also don't have "super"
*/