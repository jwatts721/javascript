let obj = {
    get propName() {
        // getter, executed on getting obj.propName
    },
    set propName(value) {
        // setter, executed on obj.propName = value
    }
}

{
    let user = {
        name: "John",
        surname: "Smith",
        get fullName() {
            return `${this.name} ${this.surname}`;
        }
    }
    console.log(user.fullName);
}

{
    let user = {
        get fullName() {
            return `...`;
        }
    };

    user.fullName = "Test"; // Error (property has only a getter)
    console.log(user.fullName); // still the same value
}
{
    let user = {
        name: "Jack",
        surname: "Dempsey",
        get fullName() {
            return `${this.name} ${this.surname}`;
        },
        // The fix
        set fullName(value) {
            [this.name, this.surname] = value.split(" ");
        }
    }
    user.fullName = "Norman Baker";
    console.log(user.name);
    console.log(user.surname);
}
/**
 * Accessor descriptors
        get – a function without arguments, that works when a property is read,
        set – a function with one argument, that is called when the property is set,
        enumerable – same as for data properties,
        configurable – same as for data properties.
 */
{
    let user = {
        name: "John",
        surname: "Smith"
    };

    Object.defineProperty(user, 'fullName', {
        get() {
            return `${this.name} ${this.surname}`;
        },

        set(value) {
            [this.name, this.surname] = value.split(" ");
        }
    });

    console.log(user.fullName); // John Smith

    for (let key in user) console.log(key); // name, surname
}
/** Smarter getters/setters */
{
    let user = {
        get name() {
            return this._name;
        },

        set name(value) {
            if (value.length < 4) {
                console.log("Name is too short, need at least 4 characters");
                return;
            }
            this._name = value;
        }
    };

    user.name = "Pete";
    console.log(user.name); // Pete

    user.name = ""; // Name is too short...
}

/** Compatibility Fixes */
// Imagine we started implementing user objects using data properties name and age:
{
    function User(name, age) {
        this.name = name;
        this.age = age;
    }

    let john = new User("John", 25);

    console.log(john.age); // 25
}
// …But sooner or later, things may change. Instead of age we may decide to store birthday, because it’s more precise and convenient:
{
    function User(name, birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    let john = new User("John", new Date(1992, 6, 1));
}
// Now what to do with the old code that still uses age property?
// Let’s keep it.
// Adding a getter for age solves the problem:
{
    function User(name, birthday) {
        this.name = name;
        this.birthday = birthday;

        // age is calculated from the current date and birthday
        Object.defineProperty(this, "age", {
            get() {
                let todayYear = new Date().getFullYear();
                return todayYear - this.birthday.getFullYear();
            }
        });
    }

    let john = new User("John", new Date(1992, 6, 1));

    console.log(john.birthday); // birthday is available
    console.log(john.age);      // ...as well as the age
}