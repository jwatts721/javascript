// [[Prototype]]
// - In JavaScript, objects have a special property called [[Prototype]] that points to another object.
// - When a new object is created, it gets a copy of the [[Prototype]] object.
// - __proto__ is a bit outdated and is used for historical purposes only.
{
    let animal = {
        eats: true
    };
    let rabbit = {
        jumps: true
    };
    console.log(rabbit.eats); // undefined
    rabbit.__proto__ = animal; // extends animal to rabbit
    console.log(rabbit.eats); // true, after proto assignment
    console.log(rabbit.jumps); // true
}
{
    let animal = {
        eats: true,
        walk() {
            console.log('Animal walks');
        }
    }

    let rabbit = {
        jumps: true,
        __proto__: animal
    }

    // walk is take from the prototype
    rabbit.walk(); // Animal walks
}
{
    let animal = {
        eats: true,
        walk() {
            console.log('Animal walks');
        }
    };
    let rabbit = {
        jumps: true,
        __proto__: animal
    }
    let longEar = {
        earLength: 10,
        __proto__: rabbit
    }

    // walk is take from the prototype chain
    longEar.walk(); // Animal walks
    console.log(longEar.jumps); // true (from rabbit)
}

/**
 * F.prototype
 * JavaScript had prototypal inheritance before ES6. But back then there was no access to it. 
 * The only thing that worked reliably was a "prototype" property on the constructor function.
 */
{
    let animal = {
        eats: true,
    };

    function Rabbit(name) {
        this.name = name;
    }

    Rabbit.prototype = animal;

    let rabbit = new Rabbit('White Rabbit'); // rabbit.__proto__ = animal;
    console.log(rabbit.eats); // true
    console.log(rabbit.name);
}
// Default F.prototype, consructor property
// - Every function has the "prototype" property, even if we don't supply it.
{
    function Rabbit() { };
    console.log(Rabbit.prototype); // {constructor: Rabbit}
    console.log(Rabbit.prototype.constructor === Rabbit); // true
    let rabbit = new Rabbit("White Rabbit");
    console.log(rabbit.constructor === Rabbit); // true
    let rabbit2 = new rabbit.constructor("Black Rabbit"); // handy when we don't know what constructor to use, eg: it comes from a 3rd party library
}
// IMPORTANT: JavaScript does not ensure the right "constructor" value
{
    function Rabbit() { }
    Rabbit.prototype = {
        jumps: true,
    };
    let rabbit = new Rabbit();
    console.log(rabbit.constructor === Rabbit); // false
}
// - SOLUTION: We don't need to overwrite Rabbit.prototype completely, we can just add a new property to it.
{
    function Rabbit() { }
    Rabbit.prototype.jumps = true;
    let rabbit = new Rabbit();
    console.log(rabbit.constructor === Rabbit); // true
}
// Alternative: Recreate the constructor property manually
{
    function Rabbit() { }
    Rabbit.prototype = {
        jumps: true,
        constructor: Rabbit
    };
    let rabbit = new Rabbit();
    console.log(rabbit.constructor === Rabbit); // true
}