/**
 * Native prototypes
 */
{
    // String type
    let obj = {};
    console.log(obj.__proto__ === Object.prototype); // true
    console.log(obj.toString() === obj.__proto__.toString); // true
    console.log(obj.toString === Object.prototype.toString); // true

    // Array type
    let arra = [1, 2, 3];

    // Inhertis from Array.prototype
    console.log(arra.__proto__ === Array.prototype); // true

    // Then from Object.prototype
    console.log(arra.__proto__.__proto__ === Object.prototype); // true

    // and null on the top
    console.log(arra.__proto__.__proto__.__proto__ === null); // true

    // Some methods in prototypes may overlap
    let arr = [1, 2, 3];
    console.log(arr); // 1, 2, 3 <-- the result of Array.prototype.toString

    // Date Type
    let date = new Date();
    console.log(date.__proto__ === Date.prototype); // true

    // Function Type
    function f() { };
    console.log(f.__proto__ === Function.prototype); // true
    console.log(f.__proto__.__proto__ === Object.prototype); // true
}

/*****
 * Primitives
 * -- The most intricate thing hapens with strings, numbers and booleans
 * -- They are not objects. But if we try to access their properties, temporary wrapper objects are created using built-in String, Number and Boolean constructors.
 * -- There is no wrapper for "null" or "undefined".
 */
{
    // Changing native prototypes
    String.prototype.show = function () { console.log(this); };

    "BOOM!".show(); // BOOM!

    /**
     * During the process of development, we may have ideas for new built-in methods we’d like to have, and we may be tempted to add them to native prototypes. 
     * But that is generally a bad idea.
     */
}

/*****
 * Polyfills
 * -- Polyfills are a way to add new methods to native prototypes.
 * -- This is the only case where it is acceptable to modify native prototypes.
 * 
 * -- Polyfill is a term for making a substitution for a method that exists in the JavaScript specification, but is not yet supported in a particular
 * -- JavaScript engine or browser.
 */
if (!String.prototype.repeat) { // if there's no such method
    // add it to the prototype chain
    String.prototype.repeat = function (count) {
        //let result = '';
        //for (let i = 0; i < count; i++) {
        //    result += this;
        //}
        //return result;
        return new Array(count).join(this);
    }
}
console.log("La".repeat(3)); // LaLaLa

/**
 * Borrowing from prototypes
 * -- We can borrow methods from other prototypes.
 */
let obj = {
    0: "Hello",
    1: "World",
    length: 2
}
obj.join = Array.prototype.join;
console.log(obj.join(', ')); "Hello, World"
