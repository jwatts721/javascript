/*****
 * __proto__ is a reserved word in javascript which is considered outdated.
 * 
 * Here are some modern methods:
 * - Object.create
 * - Object.setPrototypeOf
 * - Object.getPrototypeOf
 */
{
    let animal = {
        eats: true
    };

    let rabbit = Object.create(animal);
    console.log(rabbit.eats); // true

    console.log(Object.getPrototypeOf(rabbit) === animal); // true
    Object.setPrototypeOf(rabbit, {}); // change the prototype of rabbit to {}
}
// Object.create has an optional second argument
{
    let animal = {
        eats: true
    }
    let rabbit = Object.create(animal, {
        jumps: {
            value: true
        }
    }); 
    console.log(rabbit.jumps); // true
    
    // The descriptors can be used to perform an object cloning
    let clone = Object.create(Object.getPrototypeOf(rabbit), Object.getOwnPropertyDescriptors(rabbit));
    console.log(clone.jumps); // true
}
// Don't change [[Prototype]] of an object if speed matters.
// -- Changing a prototype on the fly  is a very slow operation and it breaks internal optimizations for object property access operations

/*****
 * "Very Plain" objects
 * -- As we know, objects can be used as associative arrays to store key/value pairs
 * -- …But if we try to store user-provided keys in it (for instance, a user-entered dictionary), 
 * -- we can see an interesting glitch: all keys work fine except "__proto__".
 */
{
    let obj = {};
    let key = "test-key";
    obj[key] = "test-value";
    console.log(obj[key]); // "test-value"

    key = "another-key";
    obj[key] = "another-value";
    console.log(obj[key]); // "test-value"

    key = "__proto__";
    obj[key] = "new-value";
    console.log(obj[key]); // not "new-value", but displays the object

    // __proto__ is a way to access [[Prototype]], it is not [[Prototype]] itself
}
// A trick
{
    let obj = Object.create(null); // <-- creates an empty object without a prototype ([[Prototype]] is null)
    let key = "__proto__";
    obj[key] = "new-value";

    console.log(obj[key]); // "new-value"
}
// The Object.create(null) lack built-in object methods e.g. toString
// -- but that's ok for associative arrays
{
    let chineseDictionary = Object.create(null);
    chineseDictionary.hello = "你好";
    chineseDictionary.bye = "再见";
    
    console.log(Object.keys(chineseDictionary)); // hello,bye
}

// Exercises
{
    // Add a toString method to the dictionary object
    //let dictionary = Object.create(null);

    // Solution
    let dictionary = Object.create(null, {
        toString: {
            value: function() {
                return Object.keys(this).join(", ");
            }
        }
    });

    // add some data
    dictionary.apple = "Apple";
    dictionary.__proto__ = "test"; // __proto__ is a regular property key here

    // only apple and __proto__ are in the loops
    for (let key in dictionary) {
        console.log(key);
    }

    console.log(dictionary.toString()); // "apple, __proto__"
}

