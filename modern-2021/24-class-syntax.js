/**
 * The old style constructor is "new function".
 * The modern approache is now using the "class" consruct.
 */
// Basic Syntax
class Person {
    constructor(name, age) { 
        this.name = name;
        this.age = age;
    }
    getName() { return this.name; }
    getAge()  { return this.age; }
}
let person = new Person('John', 34);
console.log(person.getName()); // John
console.log(person.getAge());  // 34

// In JavaScript, a class is a type of function
console.log(typeof Person); // function

// Unlike functions, all code in classes are in "use strict" mode
// Class properties are not enumerable in for in loops
// Classes must be accessed using the "new" keyword

// Class Expressions
// - Just like functions, classes can be created using expressions
let User = class UserClass {
    sayHi() { console.log('Hi!'); }
}
new User().sayHi(); // Hi!

// Getters/Setters
{
    class User {
        constructor(name) {
            this.name = name; // invokes the setter
        }
        get name () { return this._name; } // Important to use _name
        
        set name(value) { 
            if (value.length < 3) {
                console.log('Name is too short');
                return;
            }
            this._name = value; 
        }
    }

    let user = new User('John');
    console.log(user.name); // John
    user = new User("");
}

// Computed Names
{
    class User {
        ['say' + 'Hi']() {
            console.log('Hello');
        }
    }
    new User().sayHi(); // Hello
}

// Class fields
// -- Allows us to add nay properties
// -- old browsers may need polyfill
// -- support in Node >= 12.0.0
{
    class User {
        name = "John";

        sayHi() {
            console.log(`Hello, ${this.name}`);
        }
    }
    new User().sayHi(); // Hello, John
}

// Function binding with class fiends
    // The Problem with "this"
{
    class Button {
        constructor(text) {
            this.text = text || 'Hello';
        }
        click() {
            console.log(this.text + ' World!');
        }
    }
    let btn = new Button("Hi");

    setTimeout(btn.click, 1000); // undefined World!

    // The problejm is called "losing this"
}
    // Solution 1
{
    // Pass a wrapper function in set timeout
    class Button {
        constructor(text) {
            this.text = text || 'Hello';
        }
        click() {
            console.log(this.text + ' World!');
        }
    }
    let btn = new Button("Hi");

    setTimeout(() => btn.click(), 1000); // Hi World!
}
    // Solution 2
{
    // Bind the method to object in the constructor
    class Button {
        constructor(text) {
            this.text = text || 'Hello';
        }
        // Binding to object
        click = () => {
            console.log(this.text + ' World!');
        }
    }
    let btn = new Button("Hey");

    setTimeout(btn.click, 1000); // Hey World!
}
