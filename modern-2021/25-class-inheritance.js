/**
 * The "extends" keyword
 */
{
    class Animal {
        constructor(name) {
            this.speed = 0;
            this.name = name;
        }
        run(speed) {
            this.speed = speed;
            console.log(`${this.name} runs with speed ${this.speed}.`);
        }
        stop() {
            this.speed = 0;
            console.log(`${this.name} stands still.`);
        }
    }

    let animal = new Animal("My animal");

    class Rabbit extends Animal {
        hide() {
            console.log(`${this.name} hides.`);
        }
    }
    let rabbit = new Rabbit("White Rabbit");
    rabbit.run(5);
    rabbit.hide();
}
// Any expression is allowed after extends
{
    function f(phrase) {
        return class {
            sayHi() {
                console.log(phrase);
            }
        }
    }

    class User extends f("Hello Jack") { }
    new User().sayHi(); // Hello Jack
}
/**
 * Overriding a method
 */
{
    class Animal {

        constructor(name) {
            this.speed = 0;
            this.name = name;
        }

        run(speed) {
            this.speed = speed;
            console.log(`${this.name} runs with speed ${this.speed}.`);
        }

        stop() {
            this.speed = 0;
            console.log(`${this.name} stands still.`);
        }

    }

    class Rabbit extends Animal {
        hide() {
            console.log(`${this.name} hides!`);
        }

        stop() {
            super.stop(); // calls the parent method
            this.hide(); // and then hides
        }
    }

    let rabbit = new Rabbit("White Rabbit");
    rabbit.run(5);
    rabbit.stop();
}
/** Reminder: Arrow functions have no super */
{
    class Animal {

        constructor(name) {
            this.speed = 0;
            this.name = name;
        }

        run(speed) {
            this.speed = speed;
            console.log(`${this.name} runs with speed ${this.speed}.`);
        }

        stop() {
            this.speed = 0;
            console.log(`${this.name} stands still.`);
        }

    }

    class Rabbit extends Animal {
        hide() {
            console.log(`${this.name} hides!`);
        }

        stop() {
            setTimeout(() => super.stop(), 1000); // calls the parent method since it does not reference super, takes from outer function
        }
    }
    let rabbit = new Rabbit("White Rabbit");
    rabbit.run(5);
    rabbit.stop();
}
// Unexpected super
{
    class Animal {

        constructor(name) {
            this.speed = 0;
            this.name = name;
        }

        run(speed) {
            this.speed = speed;
            console.log(`${this.name} runs with speed ${this.speed}.`);
        }

        stop() {
            this.speed = 0;
            console.log(`${this.name} stands still.`);
        }

    }

    class Rabbit extends Animal {
        hide() {
            console.log(`${this.name} hides!`);
        }

        stop() {
            setTimeout(function () { 
                // super.stop(); <-- this produces an error, cannot reference super in outer function
             }, 1000);
        }
    }
    let rabbit = new Rabbit("White Rabbit");
    rabbit.run(5);
    rabbit.stop();
}
/**
 * Overriding Constructor
 */
{
    class Animal {
        constructor(name) {
            this.speed = 0;
            this.name = name;
        }
    }
    class Rabbit extends Animal {
        // Problem
        constructor(name, earLength) {
            this.speed = 0;
            this.name = name;
            this.earLength = earLength;
        }
    }
    //let rabbit = new Rabbit("White Rabbit", 10); // Error: Cannot set property 'earLength' of undefined
}
{
    class Animal {
        constructor(name) {
            this.speed = 0;
            this.name = name;
        }
    }
    class Rabbit extends Animal {
        // Solution
        constructor(name, earLength) {
            super(name); // <-- This call to super is required!
            this.earLength = earLength;
        }
    }
    let rabbit = new Rabbit("White Rabbit", 10); // Error: Cannot set property 'earLength' of undefined
}
// Parent fields keep their own values wth default constructors
{
    class Animal { 
        name = "Animal";
        constructor(name) {
            console.log(this.name); // Animal
        }
    };
    class Rabbit extends Animal {
        name = "Rabbit";
    }   
    new Animal(); // Animal
    new Rabbit(); // Animal <-- uses the field from the parent, not the overridden one
}
// However we can override a method that will change the value of the field
{
    class Animal { 
        name = "Animal";
        constructor(name) {
            this.showName();
        }
        showName() {
            console.log(this.name); // Animal
        }
    };
    class Rabbit extends Animal {
        name = "Rabbit";
        showName() {
            this.name = "Rabbit";
            console.log(this.name);
        }
    }   
    new Animal(); // Animal
    new Rabbit(); // Rabbit
}