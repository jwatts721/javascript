{
    class User {
        static staticMethod() {
            console.log(this === User);
        }
    }
    User.staticMethod(); // true
}
// The same as assigning the property directly 
{
    class User { }
    User.staticMethod = function () {
        console.log(this === User);
    }
    User.staticMethod(); // true
}
// The value of "this" inside a static method is the class constructor itself.
// Usually, static methods are used to implement functions that belong to the class, but not to any particular
// object of the class.
{
    class Article {
        constructor(title, date) {
            this.title = title;
            this.date = date;
        }
        static compare(articleA, articleB) {
            return articleA.date - articleB.date;
        }
    }
    let articles = [
        new Article('Angular', new Date(2020, 0, 1)),
        new Article('React', new Date(2019, 11, 31)),
        new Article('Vue', new Date(2019, 11, 30)),
    ];
    articles.sort(Article.compare);
    console.log(articles);
}
// Factory method example
{
    class Article {
        constructor(title, date) {
            this.title = title;
            this.date = date;
        }
        static createTodays() {
            return new this("Today's digest", new Date());
        }
    }
    let article = Article.createTodays();
    console.log(article.title); // Today's digest
}
// Static Property example
{
    class Article {
        static publisher = "Ilya Kantor";
    }
    console.log(Article.publisher); // Ilya Kantor
}
// Static properties and methods are also inherited from the parent class.
{
    class Animal{
        static planet = "Earth";
        constructor(name, speed) {
            this.name = name;
            this.speed = speed;
        }
        run(speed = 0) {
            this.speed = speed;
            console.log(`${this.name} runs with speed ${this.speed}.`);
        }
        static compare (animalA, animalB) {
            return animalA.speed - animalB.speed;
        }
    }
    class Rabbit extends Animal {
        hide () {
            console.log(`${this.name} hides!`);
        }
    }
    let rabbits = [
        new Rabbit('White Rabbit', 10),
        new Rabbit('Black Rabbit', 5)
    ];
    rabbits.sort(Rabbit.compare);
    rabbits[0].run(); // Black Rabbit runs with speed 5
    console.log(Rabbit.planet); // Earth
}