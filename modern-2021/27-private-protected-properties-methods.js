/**
 * Protecting "waterAmount"
 */
{
    class CoffeeMachine {
        waterAmount = 0; // property is currently public

        constructor(power) {
            this._power = power;
            console.log(`Created a coffee-machine, power: ${power}`);
        }
    }
    let coffeeMachine = new CoffeeMachine(100);
    coffeeMachine.waterAmount = 200;
    console.log(coffeeMachine.waterAmount); // 200

    // In this example, waterAmount and power are public
}
/**
 * Solution
 */
{
    class CoffeeMachine {
        _waterAmount = 0; // not enforced on the language level, but by convention between programmers
        set waterAmount(value) {
            if (value < 0) {
                value = 0;
            }
            this._waterAmount = value;
        }       
        get waterAmount() {
            return this._waterAmount;
        }
        constructor(power) {
            this._power = power;
        }
    }
    let coffeeMachine = new CoffeeMachine(100);
    coffeeMachine.waterAmount = -10;
    console.log(coffeeMachine.waterAmount); // 0

    // IMPORTANT: Unfortuately, the language does not enforce the "_" character as a convention
    // So remember as a convention not to access any variable with _ prefix from outside the class
    console.log(coffeeMachine._waterAmount); // 0
}
// Read-only "power" property
{
    class CoffeeMachine {
        constructor(power) {
            this._power = power;
        }
        get power() {
            return this._power;
        }
        get power() { return this._power; }
    }
    let coffeeMachine = new CoffeeMachine(100);
    console.log(`Coffee machine power is : ${coffeeMachine.power}`); // 100
    coffeeMachine.power = 200; // Error, (no setter) - error displayed in strict mode
}
// Getter/Setter functions
{
    class CoffeeMachine {
        _waterAmount = 0;
        setWaterAmount(amount) {
            if (amount < 0) amount = 0;
            this._waterAmount = amount;
        }
        getWaterAmount() {
            return this._waterAmount;
        }
    }
    new CoffeeMachine().setWaterAmount(200);
    console.log(new CoffeeMachine().getWaterAmount()); // 200
    // Getter/Setter methods allow use to add additional parameters when needed, unlike get/set
}
// Private "#waterLimit" - a recent addition to the language and may not be widely supported as of 12/24/2021
// As opposed to the "_" prefix, the "#" character is a convention that is enforced by the language
{
    class CoffeeMachine {
        #waterLimit = 200;
        #fixWaterAmount(value) {
            if (value < 0) {
                value = 0;
            }
            if (value > this.#waterLimit) {
                value = this.#waterLimit;
            }
            return value;
        }
        setWaterAmount(value) {
            this.#waterLimit = this.#fixWaterAmount(value);
        }
        get waterAmount() {
            return this.#waterLimit;
        }
    }
    let coffeeMachine = new CoffeeMachine();
    coffeeMachine.setWaterAmount(200);
    // can't access privates from outside the class
    //coffeeMachine.#waterLimit = 100; // Error
    //coffeeMachine.#fixWaterAmount(100); // Error
    console.log(coffeeMachine.waterAmount); // 200

    // Classes that inherit from CoffeeMachine have no direct access to #waterLimit without a getter/setter
    class MegaCoffeeMachine extends CoffeeMachine {
        getWaterLimit() {
            //return this.#waterLimit; // Error, can only access from CoffeeMachine
            return super.waterAmount; // OK, access from parent class
        }
    }
    let megaCoffeeMachine = new MegaCoffeeMachine();
    console.log(megaCoffeeMachine.getWaterLimit()); // 200
}