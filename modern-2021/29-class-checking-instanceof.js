{
    class Rabbit{}
    let rabbit = new Rabbit();
    console.log(rabbit instanceof Rabbit); // true
}
// Also works with constructor functions
{
    function Rabbit(){}
    console.log(new Rabbit() instanceof Rabbit); // true
}
// And with built-in classes
{
    let arr = [1, 2, 3, 4, 5, 6, 7, 8];
    console.log(arr instanceof Array); // true
    console.log(arr instanceof Object); // true
}
// Can rephrase with prototypes
{
    class Rabbit{}
    let rabbit = new Rabbit();
    console.log(rabbit instanceof Rabbit); // true
    console.log(Rabbit.prototype.isPrototypeOf(rabbit)); // true
}
// Object affected if we change the prototype
{
    class Rabbit{}
    let rabbit = new Rabbit();
    // Changed the prototype
    Rabbit.prototype = {};
    console.log(rabbit instanceof Rabbit); // false
}
// typeof -- for primitives
{
    let value = "This is a test";
    console.log(typeof value); // string
}
// We can use this method for getting the "typeof" of an object
// -- primitives
// -- built-in objects
{
    let arr = ["a", "b", "c", "d", "e"]
    console.log({}.toString.call(arr)); // [object Array]
}
/**
 * Summary of typ-checking methods
 * 
 *              works for                                                       returns
 * typeof	    primitives	                                                    string
   {}.toString	primitives, built-in objects, objects with Symbol.toStringTag	string
   instanceof	objects	                                                        true/false
 */