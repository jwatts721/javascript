// Basic Example

let user = {
    name: 'John',
    age: 30
}
console.log(user);

let clone = {}
for (let key in user) {
    clone[key] = user[key];
}
console.log(clone);

// Built-in method example
let user2 = {
    name: 'Jane',
    age: 24
}
console.log(user2);

let clone2 = {};
Object.assign(clone2, user2);
console.log(user2);

// Merge
let user3 = {name: 'Jack'};
console.log(user3);
let permission1 = {canView: true};
let permission2 = {canEdit: true};

// copies all properties from permission1 and permission2 to new object
Object.assign(user3, permission1, permission2);
console.log(user3);


// Nested cloning
let user4 = {
    name: "John",
    sizes: {
      height: 182,
      width: 50
    }
  };
  console.log(user4.sizes.height);

    // user.sizes is an object copied by reference
let clone3 = Object.assign({}, user4);
console.log(user4.sizes === clone3.sizes);

// user and clone share the same object since sizes is by reference
user4.sizes.width++;

console.log(clone3.sizes.width);

// Constant objects can be modified
const user5 = {
    name: 'Joyce'
}
console.log(user5.name);
user5.name = 'Billy'; // Can modify a property of the object
console.log(user5.name);
user5 = {}; // Should throw an error, can't modify the object