/**
 * In JavaScript we can only:
 * - Inherit from a single object
 * - There can only be one [[Prototype]] for an object
 * - A class may extend only one other class
 * 
 * To bypass this limitation, we can use the "mixins"
 */
// Mixins - combining the functionality of multiple objects
{
    // Mixin
    let sayHiMixin = {
        sayHi() {
            console.log(`Hello ${this.name}!`);
        },
        sayBye() {
            console.log(`Bye ${this.name}`);
        }
    };
    // Usage
    class User {
        constructor(name) {
            this.name = name;
        }
    }
    // Copy the methods
    Object.assign(User.prototype, sayHiMixin);
    
    // now User can say hi
    new User("Dude").sayHi(); // Hello Dude!

    // This is NOT inheritance, but just simple method copying - similar to the way traits work in PHP
}
// Mixins can make use of inheritance inside themselves
// For instance, here sayHiMixin inherits from sayMixin
{
    let sayMixin = {
        say(phrase) {
            console.log(phrase);
        }
    }

    let sayHiMixin = {
        __proto__: sayMixin, /// or we could use Object.setPrototypeOf to set the prototype here
        sayHi() {
            // call parent method
            super.say(`Hello ${this.name}!`);
        },
        sayBye() {
            super.say(`Bye ${this.name}`);
        }
    };
    class User {
        constructor(name) {
            this.name = name;
        }
    }
    // Copy the methods
    Object.assign(User.prototype, sayHiMixin);
    // Now User can say hi
    new User("Dude").sayHi(); // Hello Dude!
}