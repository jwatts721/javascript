// Try/Catch is synchronous
{
    try {
        setTimeout(function () {
            //  noSuchVariable; // script will die here
        }, 1000);
    } catch (err) {
        console.log("won't work");
    }

    try {
        noSuchVariable;
    } catch (err) {
        console.log("Error catuch - " + err.message); // <-- this works
    }
}
// Error Object
{
    try {
        failCondition;
    } catch (err) {
        console.log("Name: " + err.name);
        console.log("Message: " + err.message);
        console.log("Stack: " + err.stack);
    }
}

// Throwing our own errors
{
    let json = '{ "age": 30 }'; // incomplete data

    try {

        let user = JSON.parse(json); // <-- no errors

        if (!user.name) {
            throw new SyntaxError("Incomplete data: no name"); // (*)
        }

        console.log(user.name);

    } catch (err) {
        console.log("JSON Error: " + err.message); // JSON Error: Incomplete data: no name
    }
}
/**
 * Rethrowing
 * - try/catch should only process errors it knows about and retrow any others
*/
{
    let json = '{ "age": 30 }'; // incomplete data
    try {

        let user = JSON.parse(json);

        if (!user.name) {
            throw new SyntaxError("Incomplete data: no name");
        }

        blabla(); // unexpected error

        console.log(user.name);

    } catch (err) {

        if (err instanceof SyntaxError) {
            console.log("JSON Error: " + err.message);
        } else {
            throw err; // rethrow (*)
        }

    }
}
// Try/Catch/Finally
// -- Finally is always executed regardless of the try/catch state. Ideal for cleanup
{
    let num = 35;

    let diff, result;

    function fib(n) {
        if (n < 0 || Math.trunc(n) != n) {
            throw new Error("Must not be negative, and also an integer.");
        }
        return n <= 1 ? n : fib(n - 1) + fib(n - 2);
    }

    let start = Date.now();

    try {
        result = fib(num);
    } catch (err) {
        result = 0;
    } finally {
        diff = Date.now() - start;
    }

    console.log(result || "error occurred");

    console.log(`execution took ${diff}ms`);
}
// Finally works for any exit from try/catch
{
    function func() {

        try {
            return 1;

        } catch (err) {
            /* ... */
        } finally {
            console.log('finally');
        }
    }

    console.log(func()); // first works console.log from finally, and then this one
}
/**
 * Extending Errors
 */
{
    class ValidationError extends Error {
        constructor(message) {
            super(message);
            this.name = "ValidationError";
        }
    }

    // Usage
    function readUser(json) {
        let user = JSON.parse(json);

        if (!user.age) {
            throw new ValidationError("No field: age");
        }
        if (!user.name) {
            throw new ValidationError("No field: name");
        }

        return user;
    }

    // Working example with try..catch

    try {
        let user = readUser('{ "age": 25 }');
    } catch (err) {
        if (err instanceof ValidationError) {
            console.log("Invalid data: " + err.message); // Invalid data: No field: name
        } else if (err instanceof SyntaxError) { // (*)
            console.log("JSON Syntax Error: " + err.message);
        } else {
            throw err; // unknown error, rethrow it (**)
        }
    }
}
// Further Inheritance
{
    class ValidationError extends Error {
        constructor(message) {
            super(message);
            this.name = "ValidationError";
        }
    }

    class PropertyRequiredError extends ValidationError {
        constructor(property) {
            super("No property: " + property);
            this.name = "PropertyRequiredError";
            this.property = property;
        }
    }

    // Usage
    function readUser(json) {
        let user = JSON.parse(json);

        if (!user.age) {
            throw new PropertyRequiredError("age");
        }
        if (!user.name) {
            throw new PropertyRequiredError("name");
        }

        return user;
    }

    // Working example with try..catch

    try {
        let user = readUser('{ "age": 25 }');
    } catch (err) {
        if (err instanceof ValidationError) {
            console.log("Invalid data: " + err.message); // Invalid data: No property: name
            console.log(err.name); // PropertyRequiredError
            console.log(err.property); // name
        } else if (err instanceof SyntaxError) {
            console.log("JSON Syntax Error: " + err.message);
        } else {
            throw err; // unknown error, rethrow it
        }
    }
}