/**
 * Basic Callback examples
 */
{
    function doTask(task, callback) {
        console.log(`Starting ${task}`);
        callback();
    }
    doTask("Task 1", function () {
        console.log('Completing the callback');
    });

    // Using arrow functions
    doTask("Task 2", () => {
        console.log('Completing the callback with arrow functions');
    });
}
// PROBLEM: Nesting from hell
// - Pyramid of Doom
function loadScript(src, callback) {
    console.log("Browser loads script called..." + src);
    callback();
}
{
    loadScript('1.js', function (error, script) {

        if (error) {
            handleError(error);
        } else {
            // ...
            loadScript('2.js', function (error, script) {
                if (error) {
                    handleError(error);
                } else {
                    // ...
                    loadScript('3.js', function (error, script) {
                        if (error) {
                            handleError(error);
                        } else {
                            // ...continue after all scripts are loaded (*)
                        }
                    });

                }
            });
        }
    });
}
// Solution:
// Branch each one into its own function
{
    loadScript('1.js', step1);

    function step1(error, script) {
        if (error) {
            handleError(error);
        } else {
            // ...
            loadScript('2.js', step2);
        }
    }

    function step2(error, script) {
        if (error) {
            handleError(error);
        } else {
            // ...
            loadScript('3.js', step3);
        }
    }

    function step3(error, script) {
        if (error) {
            handleError(error);
        } else {
            // ...continue after all scripts are loaded (*)
        }
    }
}