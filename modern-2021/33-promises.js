/**
 * A promise is a special JavaScript object that linkes the executing code and the resulting client code together.
 * Unlike callbacks, a promise can only have one result. A callback may be called many times
 */
// Successful Promise
{
    let promise = new Promise(function (resolve, reject) {
        // the function is executed automatically when the promise is constructed

        // after 1 second signal that the job is done with the result "done"
        setTimeout(() => resolve("done"), 1000);
    });
}
// Failed Promise
{
    let promise = new Promise(function (resolve, reject) {
        // after 1 second signal that the job is finished with an error
        //setTimeout(() => reject(new Error("Whoops!")), 1000);
    });
}
// There should only be one resolve or one reject, any subsequent calls of resolve or reject are ignored
{
    let promise = new Promise(function (resolve, reject) {
        resolve("done");

        reject(new Error("…")); // ignored
        setTimeout(() => resolve("…")); // ignored
    });
}
// Immediately calling resolve/reject
{
    // In practice, the executor usually does something asynchronously and calls resolve, reject after some time,
    // but this is not required
    let promise = new Promise(function (resolve, reject) {
        // not taking our time to do the job
        resolve(123); // immediately give the result: 123
    });
}
// The state and result of promises are internal and cannot be access directly.
// We use .then/.catch/.finally to declared what happens after a promise is fullfilled or rejected.
/**
 * Consumers: then, catch, finally
 */
{
    // Producer
    let promise = new Promise(function (resolve, reject) {
        setTimeout(() => resolve("done!"), 1000);
    });

    // Consumer
    // resolve runs the first function in .then
    promise.then(
        result => console.log(result), // shows "done!" after 1 second
        error => console.log(error) // doesn't run
    );
}
// Again, but with failure
{
    let promise = new Promise(function (resolve, reject) {
        setTimeout(() => reject(new Error("Whoops!")), 1000);
    });

    // reject runs the second function in .then
    promise.then(
        result => console.log(result), // doesn't run
        error => console.log(error) // shows "Error: Whoops!" after 1 second
    );
}
// Using Catch
{
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => reject(new Error("Whoops! with Catch")), 1000);
    });

    // .catch(f) is the same as promise.then(null, f)
    promise.catch(console.log); // shows "Error: Whoops!" after 1 second
}
// Using Finally
{
    new Promise((resolve, reject) => {
        setTimeout(() => resolve("result"), 2000)
    })
        .finally(() => console.log("Promise ready after 2 seconds"))
        .then(result => console.log(result)); // <-- .then handles the result
}
// -- With error
{
    new Promise((resolve, reject) => {
        throw new Error("error");
    })
        .finally(() => console.log("Promise ready after error"))
        .catch(err => console.log(err));  // <-- .catch handles the error object
}

// Promise handling is asynchronous. When the order matters, how is this handled.
{
    new Promise((resolve, reject) => {
        setTimeout(() => resolve("1a - promise done!"), 1000);
    }).then(result => console.log(result));
    console.log('1a - Code finished');

    // How do we ensure if we wanted code-finished after promise done!
    // - Simple: We put it into thte queue iwht .then
    new Promise((resolve, reject) => {
        setTimeout(() => resolve("1b - promise done!"), 1000);
    }).then(result => console.log(result))
    .then(() => console.log('1b - Code finished'));
}