/**
 * Promise Chaining Example
 */
new Promise(function (resolve, reject) {

    setTimeout(() => resolve(1), 1000); // (*)

}).then(function (result) { // (**)

    console.log(result); // 1
    return result * 2;

}).then(function (result) { // (***)

    console.log(result); // 2
    return result * 2;

}).then(function (result) {

    console.log(result); // 4
    return result * 2;

});

/**
 * Returning Promises
 */
new Promise(function (resolve, reject) {

    setTimeout(() => resolve(1), 1000);

}).then(function (result) {

    console.log(result); // 1

    return new Promise((resolve, reject) => { // (*)
        setTimeout(() => resolve(result * 2), 1000);
    });

}).then(function (result) { // (**)

    console.log(result); // 2

    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(result * 2), 1000);
    });

}).then(function (result) {

    console.log(result); // 4

});

// Thenables
// A thenable is an object that has a then method.
// A thenable can be used as a promise.
class Thenable {
    constructor(num) {
        this.num = num;
    }
    then(resolve, reject) {
        console.log(resolve); // function() { native code }
        // resolve with this.num*2 after the 1 second
        setTimeout(() => resolve(this.num * 2), 1000); // (**)
    }
}

new Promise(resolve => resolve(1))
    .then(result => {
        return new Thenable(result); // (*)
    })
    .then(console.log); // shows 2 after 1000ms