// Implicit try...catch
new Promise((resolve, reject) => {
    throw new Error("Whoops!");
}).catch(console.log); // Error: Whoops!

// Works exactly the same as this
new Promise((resolve, reject) => {
    reject(new Error("Whoops!"));
}).catch(console.log); // Error: Whoops!

// Retrhrowing
// the execution: catch -> then
new Promise((resolve, reject) => {

    throw new Error("Whoops!");

}).catch(function (error) {

    console.log("The error is handled, continue normally");

}).then(() => console.log("Next successful handler runs"));

// The below example, the handler(*) can't handle the error, so jumps to the next catch
// the execution: catch -> catch
new Promise((resolve, reject) => {

    throw new Error("Whoops!");

}).catch(function (error) { // (*)

    if (error instanceof URIError) {
        // handle it
    } else {
        console.log("Can't handle such error");

        throw error; // throwing this or another error jumps to the next catch
    }

}).then(function () {
    /* doesn't run here */
}).catch(error => { // (**)

    console.log(`The unknown error has occurred: ${error}`);
    // don't return anything => execution goes the normal way

});

/**
 * In the case of an error, the promise is rejected and the execution should jump to the closes reject handler (catch).
 * Here is an example without one and the error gets stuck, no further execution of code
 */
new Promise((resolve, reject) => {
    noSuchFunction(); // Error here (no such function);
}).then(() => {
    /* doesn't run here */
}) // Error: Uncaught ReferenceError: noSuchFunction is not defined
.finally(() => {
    console.log('Finally still happens...');
})