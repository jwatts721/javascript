/**
 * There are 6 static methods in the Promise class
 */
/**
 * Promise.all(iterable)
 * - all or nothing
 */
Promise.all([
    new Promise(resolve => setTimeout(() => resolve(1), 3000)), // 1
    new Promise(resolve => setTimeout(() => resolve(2), 2000)), // 2
    new Promise(resolve => setTimeout(() => resolve(3), 1000))  // 3
]).then(console.log); // 1,2,3 when promises are ready: each promise contributes an array member

// If any of the promises are rejected, the promise returned by Promise.all is rejected with an array of all the rejection reasons.
Promise.all([
    new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000)),
    new Promise((resolve, reject) => setTimeout(() => reject(new Error("Whoops!")), 2000)),
    new Promise((resolve, reject) => setTimeout(() => resolve(3), 3000))
]).catch(console.log); // Error: Whoops!
// In case of an error, other promises are ignored
// If any of the objects is not a promise, it passes to the resulting array "as is"
Promise.all([
    new Promise((resolve, reject) => {
        setTimeout(() => resolve(1), 1000)
    }),
    2,
    3
]).then(console.log); // 1, 2, 3

/**
 * Promise.allSettled(iterable)
 * - Waits for all promises to settle, regardless of the result
 */
Promise.allSettled([
    new Promise(resolve => setTimeout(() => resolve(1), 3000)), // 1
    new Promise((resolve, reject) => setTimeout(() => reject(new Error("Whoops!")), 2000)),
    new Promise(resolve => setTimeout(() => resolve(3), 1000))  // 3
]).then(console.log);

/**
 * Promise.race
 * - Similar to `Promise.all, but waits for the first settled promise an get its result (or error)
 */
Promise.race([
    new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000)), // This settles first, so will be the result
    new Promise((resolve, reject) => setTimeout(() => reject(new Error("Whoops!")), 2000)),
    new Promise((resolve, reject) => setTimeout(() => resolve(3), 3000))
]).then(console.log); // 1

/**
 * Promise.any
 * - Similar to Promise.race, but waits only for the first fullfilled promise and gets its result
 */
Promise.any([
    new Promise((resolve, reject) => setTimeout(() => reject(new Error("Whoops!")), 1000)),
    new Promise((resolve, reject) => setTimeout(() => resolve(1), 2000)), // is fullfiled first
    new Promise((resolve, reject) => setTimeout(() => resolve(3), 3000))
]).then(console.log); // 1

/**
 * Promise.resolve
 * Promise.reject
 * - Are not used directly in practices since async/await syntax handles this
 */
