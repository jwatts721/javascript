/**
 * Special syntax to work with promises in a more comfortable way, called "async/await"
 */
/**
 * Async ensures that the function returns a Promise
 */
{
    async function f() {
        return 1;
    }
    f().then(console.log); // 1
}
// Same as this
{
    function f() {
        return Promise.resolve(1);
    }
    f().then(console.log); // 1
}

/**
 * Await
 * - works only inside an async function
 */
{
    async function f() {
        let promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve("promise done!"), 1000);
        });
        let result = await promise;
        console.log(result); // "promise done!"
    };
    f();
}
// Await CANNOT be used in regular functions
{
    function f() {
        let promise = Promise.resolve(1);
        //let result = await promise; // <-- syntax error
    }
}

// Await accepts "thenables"
{
    class Thenable {
        constructor(num) {
            this.num = num;
        }
        then(resolve, reject) {
            console.log(resolve);
            // resolve with this.num*2 after 1000ms
            setTimeout(() => resolve(this.num * 2), 1000); // (*)
        }
    }

    async function f() {
        // waits for 1 second, then result becomes 2
        let result = await new Thenable(1);
        console.log(result); // 2
    }

    f();
}

// Async class method
{
    class Waiter {
        async wait() {
            return await Promise.resolve("Waiter done waiting!");
        }
    }
    let waiter = new Waiter();
    waiter
        .wait()
        .then(console.log); // "Waiter done waiting!"
}

// Error handling
{
    async function f() {
        await Promise.reject(new Error("Whoops!"));
    }
}
// Same as this
{
    async function f() {
        throw new Error("Whoops!");
    }
}
// Error handling example
{
    async function f() {
        try {
            await Promise.reject(new Error("Whoops!"));
        } catch (e) {
            console.log(e); // Error: Whoops!
        }
    };
    f();
}
// Works well with Promise.all
{
    async function f() {
        let results = await Promise.all([
            Promise.resolve(1),
            Promise.resolve(2),
        ]);
        console.log(results); // [1, 2]
    }
    f();
}
