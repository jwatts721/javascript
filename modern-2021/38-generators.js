/**
 * Regular functions return only once, single value (or nothing).
 * 
 * Generators can return ("yield") multiple values, and they can be one after another, on demand.
 * They work great with iterables, allowing to create data streams with ease.
 * - When a generator is called, it doesn't run code, rather it returns a
 *   special object called "generator object" to manage the execution.
 */
{
    function* generateSequence() {
        yield 1;
        yield 2;
        return 3;
    }

    let generator = generateSequence();
    console.log(generator); // Generator { }

    let one = generator.next();
    console.log(one); // { value: 1, done: false }\

    let two = generator.next();
    console.log(two); // { value: 2, done: false }

    let three = generator.next();
    console.log(three); // { value: 3, done: true }
}
// Generators are iterable
{
    function* generateSequence() {
        yield 1;
        yield 2;
        return 3;
    }
    let generator = generateSequence();
    for (let value of generator) {
        console.log(value);
    }
    // Problem: Ignores 3, because done is true, so we must return it with yield so done remains false

    function* generateSequence2() {
        yield 1;
        yield 2;
        yield 3;
    }
    for (let value of generateSequence2()) {
        console.log(value);
    }
}
// We can also use the spread syntax
{
    function* generateSequence() {
        yield 1;
        yield 2;
        yield 3;
    }
    let sequence = [0, ...generateSequence()];
    console.log(sequence); // [0, 1, 2, 3]
}
// Generators can generate values forever - so we must  be sure to be able to break out of the sequence
/**
 * Using generators for iterables
 * -- example of iterable range
 */
{
    let range = {
        from: 1,
        to: 5,

        // for..of range calls this method once in the very beginning
        [Symbol.iterator]() {
            // ...it returns the iterator object:
            // onward, for..of works only with that object, asking it for next values
            return {
                current: this.from,
                last: this.to,

                // next() is called on each iteration by the for..of loop
                next() {
                    // it should return the value as an object {done:.., value :...}
                    if (this.current <= this.last) {
                        return { done: false, value: this.current++ };
                    } else {
                        return { done: true };
                    }
                }
            };
        }
    };

    // iteration over range returns numbers from range.from to range.to
    console.log([...range]); // 1,2,3,4,5
}
// with generators
{
    let range = {
        from: 1,
        to: 5,

        *[Symbol.iterator]() { // a shorthand for [Symbol.iterator]: function*()
            for (let value = this.from; value <= this.to; value++) {
                yield value;
            }
        }
    };

    console.log([...range]); // 1,2,3,4,5
}

/**
 * Generator composition
 * -- special feature of generators that allows to transparently "embed" generators in each other
 * -- yield* delegates the execution to another generator
 */
{ // Compound generator -- ie: password generation
    function* generateSequence(start, end) {
        for (let i = start; i <= end; i++) yield i;
    }

    function* generatePasswordCodes() {

        // 0..9
        yield* generateSequence(48, 57);

        // A..Z
        yield* generateSequence(65, 90);

        // a..z
        yield* generateSequence(97, 122);

    }

    let str = '';

    for (let code of generatePasswordCodes()) {
        str += String.fromCharCode(code);
    }

    console.log(str); // 0..9A..Za..z
}
// Result is the same as if we inlined the code from nested generators
{
    function* generateSequence(start, end) {
        for (let i = start; i <= end; i++) yield i;
    }

    function* generateAlphaNum() {

        // yield* generateSequence(48, 57);
        for (let i = 48; i <= 57; i++) yield i;

        // yield* generateSequence(65, 90);
        for (let i = 65; i <= 90; i++) yield i;

        // yield* generateSequence(97, 122);
        for (let i = 97; i <= 122; i++) yield i;

    }

    let str = '';

    for (let code of generateAlphaNum()) {
        str += String.fromCharCode(code);
    }

    console.log(str); // 0..9A..Za..z
}

/**
 * "Yield" is a two-way street
 * - not only can it return the result to the outside, but it can also pass the value inside the generator
 */
{
    function* gen() {
        // Pass a question to the outer code and wait for an answer
        let result = yield "2 + 2 = ?"; // (*)

        console.log(result);
    }
    let generator = gen();
    let question = generator.next().value; // <-- yield return the value
    console.log(question); // 2 + 2 = ?
    generator.next(4); // <-- pass the answer to the generator
}
console.log('\n');
// As you can see, unlike regular functions, a generator and the calling code can exchange results by passing values in next/yield
// Another example: 
{
    function* gen() {
        let result = yield "2 + 2 = ?"; // (*)

        console.log(result); // 4

        let result2 = yield "3 * 3 = ?"; // (*)

        console.log(result2); // 9
    }
    let generator = gen();
    console.log(generator.next().value); // 2 + 2 = ?
    console.log(generator.next(4).value); // 3 * 3 = ?
    console.log(generator.next(9).done); // true 
}
// Generator.throw
// -- can also pass an error into yield
{
    function* gen() {
        try {
            let result = yield "2 + 2 = ?"; // (1)

            console.log("The execution does not reach here, because the exception is thrown above");
        } catch (e) {
            console.log(e); // shows the error
        }
    }
    let generator = gen();
    let question = generator.next().value;
    generator.throw(new Error("Something bad happened"));
}
// generator.return
// -- finishes the generator execution and return the given value
{
    function* gen() {
        yield 1;
        yield 2;
        yield 3;
    }
    const g = gen();

    let result = g.next();        // { value: 1, done: false }
    console.log(result);
    
    result = g.return('foo'); // { value: "foo", done: true }
    console.log(result);

    result = g.next();        // { value: undefined, done: true }
    console.log(result);
}