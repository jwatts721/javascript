// Method example
let user = {
    name: 'John',
    age: 30
}
user.sayHi = function() {
    console.log('Hello ' + this.name);
}
user.sayHi();

// Method Shorthand
let user2 =  {
    name: 'Jack',
    age: 34,
    sayHi() {
        console.log('Hello ' + this.name)
    }
}
user2.sayHi();

// Chaining
let ladder = {
    step: 0,
    up() { 
        this.step++;
        return this;
    },
    down() {
        this.step--;
        return this;
    },
    showStep: function () { 
        console.log(this.step);
        return this;
    }
}
// Without chaining
ladder.up();
ladder.up();
ladder.down();
ladder.showStep(); // 1

// We can chain the commands because the methods return the instance of itself
ladder.step = 0;
ladder.up().up().down().showStep();

// Optional chaining
let user3 = {
    name: 'Joe',
    age: 35,
    address: {
        street: "Berkshire Place",
        city: null
    }
};
console.log(user3?.name);
console.log(user3?.age);
console.log(user3?.address?.street);
console.log(user3?.address?.city);