let id = Symbol('id');

// Symbols cannot be converted to strings by casting
console.log(id); // Logs as the object, not the string
//console.log('Symbol: ' + id); // produces Error

// Conversion to string
console.log(id.toString());

// Or get the description
console.log(id.description);

// Symbols allow for "hidden" properties of an object
    // UNSAFE BEHAVIOR
let user = { name: 'John' };
    // Our script used the "id" property
user.id = "Our id value";

    // ... Another script also wants "id" for its purposes
user.id = "Their id value"
    
console.log(user.id);

    // SAFE BEHAVIOR with Symbols
let myId = Symbol("id");
let theirId = Symbol("id");

user[myId] = "My id value";
user[theirId] = "Their id value";

console.log(user[myId]);
console.log(user[theirId]);

// Symbols in an object literal
let someId = Symbol('someId');
let someUser = {
    name: 'John',
    [someId]: 123
}
console.log(someUser[someId]);

// Symbols are skipped by for...in
let symId = Symbol('symId');
let user2 = {
    name: 'Jack',
    age: 40,
    [symId]: 123
}
for (let key in user2) console.log(key);

// Direct access to the symbol
console.log("Direct: " + user2[symId]);

// Object.keys(user) also ignores them as part of hiding
let clone = Object.assign({}, user2);

console.log(clone[symId]);

// Global Symbols
    // read from the  global registry
id = Symbol.for('id'); // if symbol did not exist, it is created

    // read it again (maybe from another part of the code)
let idAgain = Symbol.for('id');

// The same symbol
console.log(id === idAgain);

// Reverses of Symbol.for
    // get the symbol by name
let sym = Symbol.for('name');
let sym2 = Symbol.for('id');

    // get the name by symbol
console.log(Symbol.keyFor(sym)); // name
console.log(Symbol.keyFor(sym2)); // id

// System symbols
/*
Symbol.hasInstance
Symbol.isConcatSpreadable
Symbol.iterator
Symbol.toPrimitive
Symbol.unscopables
*/
