/**
 * A Proxy object wraps another object an intercepts operations
 * -- Simple example:
 */
let target = {};
let proxy = new Proxy(target, {}); // empty handler

proxy.test = 5; // writing to proxy (1)
console.log(target.test); // 5, the property appeared in target!

console.log(proxy.test); // 5, we can read it from proxy too (2)

for (let key in proxy) console.log(key); // test, iteration works (3)

// All operations on proxy are forwarded to target

/**
 * Default value with "get" trap
 */
let numbers = [0, 1, 2];

numbers = new Proxy(numbers, {
    get(target, prop) {
        if (prop in target) {
            return target[prop];
        } else {
            return 0; // default value
        }
    }
});

console.log(numbers[1]); // 1
console.log(numbers[123]); // 0 (no such item)

// Additional example
let dictionary = {
    'Hello': 'Hola',
    'Bye': 'Adiós'
};

dictionary = new Proxy(dictionary, {
    get(target, phrase) { // intercept reading a property from dictionary
        if (phrase in target) { // if we have it in the dictionary
            return target[phrase]; // return the translation
        } else {
            // otherwise, return the non-translated phrase
            return phrase;
        }
    }
});

// Look up arbitrary phrases in the dictionary!
// At worst, they're not translated.
console.log(dictionary['Hello']); // Hola
console.log(dictionary['Welcome to Proxy']); // Welcome to Proxy (no translation)

/**
 * Validation with "set" trap
 */
{
    let numbers = [];

    numbers = new Proxy(numbers, { // (*)
        set(target, prop, val) { // to intercept property writing
            if (typeof val == 'number') {
                target[prop] = val;
                return true;
            } else {
                return false;
            }
        }
    });

    numbers.push(1); // added successfully
    numbers.push(2); // added successfully
    console.log("Length is: " + numbers.length); // 2

    //numbers.push("test"); // TypeError ('set' on proxy returned false)

    console.log("This line is never reached (error in the line above)");
}

// Iteration with "ownKeys" and "getOwnPropertyDescriptor" traps
{
    let user = {
        name: "John",
        age: 30,
        _password: "***"
    };

    user = new Proxy(user, {
        ownKeys(target) {
            return Object.keys(target).filter(key => !key.startsWith('_'));
        }
    });

    // "ownKeys" filters out _password
    for (let key in user) console.log(key); // name, then: age

    // same effect on these methods:
    console.log(Object.keys(user)); // name,age
    console.log(Object.values(user)); // John,30
}

// Ensure [[GetOwnProperty]] sets enumerable
{
    let user = {};

    user = new Proxy(user, {
        ownKeys(target) { // called once to get a list of properties
            return ['a', 'b', 'c'];
        },

        getOwnPropertyDescriptor(target, prop) { // called for every property
            return {
                enumerable: true,
                configurable: true
                /* ...other flags, probable "value:..." */
            };
        }

    });

    console.log(Object.keys(user)); // a, b, c
}

// Prevent access to properties using Proxy
{
    let user = {
        name: "John",
        _password: "***"
    };

    user = new Proxy(user, {
        get(target, prop) {
            if (prop.startsWith('_')) {
                throw new Error("Access denied");
            }
            let value = target[prop];
            return (typeof value === 'function') ? value.bind(target) : value; // (*)
        },
        set(target, prop, val) { // to intercept property writing
            if (prop.startsWith('_')) {
                throw new Error("Access denied");
            } else {
                target[prop] = val;
                return true;
            }
        },
        deleteProperty(target, prop) { // to intercept property deletion
            if (prop.startsWith('_')) {
                throw new Error("Access denied");
            } else {
                delete target[prop];
                return true;
            }
        },
        ownKeys(target) { // to intercept property list
            return Object.keys(target).filter(key => !key.startsWith('_'));
        }
    });

    // "get" doesn't allow to read _password
    try {
        console.log(user._password); // Error: Access denied
    } catch (e) { console.log(e.message); }

    // "set" doesn't allow to write _password
    try {
        user._password = "test"; // Error: Access denied
    } catch (e) { console.log(e.message); }

    // "deleteProperty" doesn't allow to delete _password
    try {
        delete user._password; // Error: Access denied
    } catch (e) { console.log(e.message); }

    // "ownKeys" filters out _password
    for (let key in user) console.log(key); // name
}

/**
 * "In range" with "has" trap
 */
let range = {
    start: 1,
    end: 10
};
range = new Proxy(range, {
    has(target, prop) { // to intercept property existence
        return prop >= target.start && prop <= target.end;
    }
});
console.log(5 in range); // true
console.log(11 in range); // false

/**
 * Wrapping functions with "apply" trap
 * - apply trap handles calling a proxy as a function
 */
{
    function delay(f, ms) {
        // return a wrapper that passes the call to f after the timeout
        return function () { // (*)
            setTimeout(() => f.apply(this, arguments), ms);
        };
    }

    function sayHi(user) {
        console.log(`Hwllo, ${user}`)
    }

    // After this wrapping, calls to sayHi will be delayed by 3 seconds
    sayHi = delay(sayHi, 3000); // (*)

    sayHi('Jack');
}

// Let's use Proxy instead of a wrapping function
{
    function delay(f, ms) {
        return new Proxy(f, {
            apply(target, thisArg, args) {
                setTimeout(() => target.apply(thisArg, args), ms);
            }
        });
    }

    function sayHi(user) {
        console.log(`Hello, ${user}!`);
    }

    sayHi = delay(sayHi, 3000);

    console.log(sayHi.length); // 1 (*) proxy forwards "get length" operation to the target

    sayHi("John"); // Hello, John! (after 3 seconds)

    // Benefits - all opertions on the proxy are forwarded to the original function
}

/**
 * Reflect is a built-in object the simplifies Proxy creation
 */
{
    let user = {};
    Reflect.set(user, 'name', 'Jack');
    console.log(user.name); // Jack
}
// Traps both get and set transparently forwarding the reading/writing operations to the object
{
    let user = {
        name: 'John'
    }

    user = new Proxy(user, {
        get(target, prop, receiver) {
            console.log(`GET ${prop}`);
            return Reflect.get(target, prop, receiver); // (1)
        },
        set(target, prop, val, receiver) {
            console.log(`SET ${prop}=${val}`);
            return Reflect.set(target, prop, val, receiver); // (2)
        }
    });

    let name = user.name; // shows "GET name"
    user.name = "Pete"; // shows "SET name=Pete"
}
// Proxying a getter
{
    let user = {
        _name: "Guest",
        get name() {
            return this._name;
        }
    };

    let userProxy = new Proxy(user, {
        get(target, prop, receiver) {
            return target[prop];
        }
    });

    console.log(userProxy.name); // Guest
}
/**
 * Proxy Limitations
 */
{
    let map = new Map();
    let proxy = new Proxy(map, {});
    //proxy.set('test', 'test'); // Error: Proxy trap 'set' not defined
}
// The fix
{
    let map = new Map();

    let proxy = new Proxy(map, {
        get(target, prop, receiver) {
            let value = Reflect.get(...arguments);
            return typeof value == 'function' ? value.bind(target) : value;
        }
    });

    proxy.set('test', 1);
    console.log(proxy.get('test')); // 1 (works!)
}
// Simliar with private class fields
{
    class User {
        #name = "Guest";

        getName() {
            return this.#name;
        }
    }

    let user = new User();

    user = new Proxy(user, {});

    //console.log(user.getName()); // Error
}
// Fixing with "get" trap
{
    class User {
        #name = "Guest";

        getName() {
            return this.#name;
        }
    }

    let user = new User();

    user = new Proxy(user, {
        get(target, prop, receiver) {
            let value = Reflect.get(...arguments);
            return typeof value == 'function' ? value.bind(target) : value;
        }
    });

    console.log(user.getName()); // Guest
}

// Proxy != target
{
    let allUsers = new Set();

    class User {
        constructor(name) {
            this.name = name;
            allUsers.add(this);
        }
    }

    let user = new User("John");

    console.log(allUsers.has(user)); // true

    user = new Proxy(user, {});

    console.log(allUsers.has(user)); // false -- because the proxy is a different object
}

// Revocable proxies
// -- a proxy that can be disabled
{
    let object = {
        data: "Valuable data"
    };

    let { proxy, revoke } = Proxy.revocable(object, {});

    // pass the proxy somewhere instead of object...
    console.log(proxy.data); // Valuable data

    // later in our code
    revoke();

    // the proxy isn't working any more (revoked)
    //console.log(proxy.data); // Error
}
// Another option - create a WeakMap that has proxy as the key and the corresponding revoke as the value, that allows
// to easily find revoke for a proxy
{
    let revokes = new WeakMap();

    let object = {
        data: "Valuable data"
    };

    let { proxy, revoke } = Proxy.revocable(object, {});

    revokes.set(proxy, revoke);

    // ..somewhere else in our code..
    revoke = revokes.get(proxy);
    revoke();

    //console.log(proxy.data); // Error (revoked)
}