/**
 * Run a code string
 */
let code = "console.log('Hello');";
eval(code);

let value = eval("2 + 2");
console.log(value); // 4

/**
 * Notes about "eval"
 * - Use very sparingly. It's a security risk.
 */