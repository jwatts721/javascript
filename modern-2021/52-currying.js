/**
 * Currying is an advanced technique of working with functions. (Available to other languages as well).
 * A transformation of functions that translates a function from callable as f(a, b, c) to f(a)(b)(c).
 * Currying doesn't call a function, it transforms it.
 */
{
    function curry(f) { // curry(f) does the currying transform
        return function (a) {
            return function (b) {
                return f(a, b);
            }
        }
    }

    // usage
    function sum(a, b) {
        return a + b;
    }

    const curriedSum = curry(sum);
    console.log(curriedSum(1)(2)); // 3
}

/**
 * Why Curry??!!
 * -- Practical Example:
 * For instance, we have the logging function log(date, importance, message) that formats and outputs the information.
 * In real projects such functions have many useful features like sending logs over the network,
 */
{
    function curry(f) { // curry(f) does the currying transform
        return function (a) {
            return function (b) {
                return function (c) {
                    return f(a, b, c);
                }
            }
        }
    }

    function log(date, importance, message) {
        console.log(`[${date.getHours()}:${date.getMinutes()}] [${importance}] ${message}`);
    }
    // Normal Use
    log(new Date(), 'INFO', 'Hello World');

    // Let's curry it
    log = curry(log);

    // Call as curry
    log(new Date())('DEBUG')('Hello World');

    // And a more sophisticated usage after curry
    let logNow = log(new Date());

    // Use It
    logNow('ERROR')('Hello World');
}
// Currying requires fixed length functions only, cannot be done wth functions that use the rest "F(...args)" parameters

// Advanced implementation
{
    // func is the function to transform
    function curried(...args) {
        if (args.length >= func.length) { // (1)
            return func.apply(this, args);
        } else {
            return function (...args2) { // (2)
                return curried.apply(this, args.concat(args2));
            }
        }
    };

    function curry(func) {

        return function curried(...args) {
            if (args.length >= func.length) {
                return func.apply(this, args);
            } else {
                return function (...args2) {
                    return curried.apply(this, args.concat(args2));
                }
            }
        };

    }

    // Usage
    function sum(a, b, c) {
        return a + b + c;
    }
    let curriedSum = curry(sum);
    console.log(curriedSum(1, 2, 3)); // 6, still callable normally
    console.log(curriedSum(1)(2, 3)); // 6, currying of 1st arg
    console.log(curriedSum(1)(2)(3)); // 6, full currying

    function multiply(a, b, c, d) {
        return a * b * c * d;
    }
    let curriedMultiply = curry(multiply);
    console.log(curriedMultiply(1, 2, 3, 4)); // 24, still callable normally
    console.log(curriedMultiply(1)(2)(3)(4)); // 24, full currying
}