/**
 * Reference Type
 * - In depth language feature
 * - Advanced topic that most developers are not familiar with
 */
// Dynamically evaluated method call can lose "this".
{
    let user = {
        name: "John",
        hi() { console.log(this.name); },
        bye() { console.log("Bye"); }
    };

    user.hi(); // works

    // now let's call user.hi or user.bye depending on the name
    (user.name == "John" ? user.hi : user.bye)(); // Error! --> undefined
}
// Reference Type Explained
/**
 *  Looking closely, we may notice two operations in obj.method() statement:

    First, the dot '.' retrieves the property obj.method.
    Then parentheses () execute it.
 */
{
    let user = {
        name: "John",
        hi() { console.log(this.name); },
    };
    // split getting and calling the method in two lines
    let hi = user.hi;
    hi(); // undefined
    
    // The result of user.hi is not a function, but a value of Reference Type.
    
    let hello = user.hi.bind(user); // Resolved with a function bind
    hello(); // John
}