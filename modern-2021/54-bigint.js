/**
 * Recent addition to the language
 * - BigInt is created by appending an "n" to the end of an interger literal or by calling the BigInt function
 */
const bigint = 1234567890123456789012345678901234567890n;

const sameBigint = BigInt("1234567890123456789012345678901234567890");

const bigintFromNumber = BigInt(10); // same as 10n


// BigInt can be used liek regular numbers
console.log(1n + 2n); // 3n
console.log(5n / 2n); // 2n

//console.log(1n + 2); // Error, cannot mix BigInt and other types
// Explicit conversion
{
    let bigint = 1n;
    let number = 2;

    // number to bigint
    console.log(bigint + BigInt(number)); // 3n

    // bigint to number
    console.log(Number(bigint) + number); // 3
}

// Unary operators not supported
let int = 10;
let bitint = 10n;

console.log(+int); // 11
//console.log(+bitint); // error

// Comparisons - works between bigints and numbers
console.log(2n > 1n); // true
console.log(2n > 1); // true

// They can be coercively compared with ==
console.log(1 == 1n); // true

// However they are not strictly ===
console.log(1 === 1n); // false