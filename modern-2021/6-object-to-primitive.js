/**
 * Conversion Rules
 * 1 - All objects are true in a boolean context. 
 *     There are only numeric and string conversions.
 * 
 * 2 - The numeric conversion happens when we subtract 
 *     objects or apply mathematical functions. 
 *     For instance, Date objects (to be covered in the 
 *     chapter Date and time) can be subtracted, and the 
 *     result of date1 - date2 is the time difference between two dates.
    
   3 - As for the string conversion – it usually happens when we output 
       an object like alert(obj) and in similar contexts.
*/

/** String */
let obj = { id: 123 }
console.log(obj);
    // String conversion
console.log('Object: ' + obj);

let anotherObj = {};
anotherObj[obj] = 123; // converts the object to string
console.log(anotherObj);
console.log(anotherObj[obj]); // 123

let test = {};
console.log(anotherObj[test]); // 123 - because the output is the same, we get the same value

/** Number */
let obj2 = "2";

    // explicit conversion
let num = Number(obj2);
console.log(num);

    // maths (except binary plus)
let n = +obj2; // unary plus
console.log(n);
let date1 = new Date();
console.log(date1);
let date2 = new Date(2020, 12, 30);
console.log(date2);
let delta = date1 - date2;
console.log(delta);

// less/greater comparison
let greater = date1 > date2;
console.log(greater);

// ToString Conversions
    // Concatenate String
let john = {
    name: 'John',
    toString () { return this.name; }
}
console.log(john + 500);

    // Number conversion
let jack = {
    name: 'Jack',
    id: "3",
    toString() { return this.id}
}
console.log(jack * 3);