// by default, objects are not iterable
let range = {
    from: 1,
    to: 5
}

// This will produce an error
try {
    for (let num of range) {
        console.log(num);
    }
} catch (e) {
    console.log(e.toString());
}

// Implement an iterable object
let range2 = {
    from: 1, 
    to: 5,
    
    [Symbol.iterator]() { 
        this.current = this.from;
        return this;
    }, 
    
    next() {
        if (this.current <= this.to) {
            return { done: false, value: this.current++ }
        }
        return { done: true };
    }
}
    // Now we are able to iterate over the object
for (let num of range2) {
    console.log(num);
}

// Strings have built in iterables, implicit example
for (let char of "Testing")
{ 
    console.log(char);
}

// Explicit String iterator example
let str = "Hello";
let iterator = str[Symbol.iterator]();
while (true) {
    let result = iterator.next();
    if (result.done) break;
    console.log(result.value);
}

// Array-Likes vs Arrays
    // We can iterate over this array
let myArray = ["Hello", "World"];
for (let item of myArray) console.log(item);

    // Looks like an array, but will produce an error when we try to iterate
let arrayLike = {0: "Hello", 1: "World", length: 2};
try {
    for (let item of arrayLike) console.log(item);
} catch (e) {
    console.log(e.toString());
}

// Or use Array.from to make it an array
let newArray = Array.from(arrayLike);
for (let item of newArray) console.log(item);

    // Alternative to iterate over an arrayLike, iterate using indexes
for (let item in arrayLike) console.log(arrayLike[item]);


// Custom mappings for iterations
let numRange = { 0: 1, 1: 2, 2: 3, 3: 4, 4: 5, length: 5 };
let numArray = Array.from(numRange, num => num * num); // custom mapping for array conversion
for (let item of numArray) console.log(item);

// Turn a string into an array using Array.from
let stringValue = 'my value';
let charArray = Array.from(stringValue);
console.log(charArray);

