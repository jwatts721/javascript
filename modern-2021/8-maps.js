/** 
 * Objects are used for storing keyed collections.
 * Arrays are used for storing ordered collections.
 * 
 * Map is a collection of keyed data items, like an Object with the difference
 * being that map allows keys of any type.
 */
 let map = new Map();

 map.set('1', 'str1');   // a string key
 map.set(1, 'num1');     // a numeric key
 map.set(true, 'bool1'); // a boolean key
 
 // remember the regular Object? it would convert keys to string
 // Map keeps the type, so these two are different:
 console.log( map.get(1)   ); // 'num1'
 console.log( map.get('1') ); // 'str1'
 
 console.log( map.size ); // 3

 console.log(map[1]); // <<-- INCORRECT, we should not treat map as a plain JavaScript object

 // Objects as Map Keys
 let john = { name: "John" };

// for every user, let's store their visits count
let visitsCountMap = new Map();

// john is the key for the map
visitsCountMap.set(john, 123);

console.log( visitsCountMap.get(john) ); // 123

// Won't work for an Oject
let jack = { name: "John" };
let ben = { name: "Ben" };

let visitsCountObj = {}; // try to use an object

visitsCountObj[ben] = 234; // try to use ben object as the key
visitsCountObj[jack] = 123; // try to use jack object as the key, ben object will get replaced

// That's what got written!
console.log( visitsCountObj[ben] ); // 123 - if this were a Map, the output would be 234
console.log( visitsCountObj['[object Object]']); // same as above


// Chaining
map.set('2', 'str2')
   .set(2, 'num2')
   .set(true, 'bool2');

console.log(map.get('2'));
console.log(map.get(2));

// Iteration over Map
let recipeMap = new Map([
    ['cucumber', 500],
    ['tomatoes', 350],
    ['onion',    50]
]);

// iterate over keys (vegetables)
for (let vegetable of recipeMap.keys()) {
console.log(vegetable); // cucumber, tomatoes, onion
}

// iterate over values (amounts)
for (let amount of recipeMap.values()) {
console.log(amount); // 500, 350, 50
}

// iterate over [key, value] entries
for (let entry of recipeMap) { // the same as of recipeMap.entries()
console.log(entry); // cucumber,500 (and so on)
}

// Map preserves the order of insertion unlike Object
// Map has built-in foreach
recipeMap.forEach( (value, key, map) => {
    console.log(`${key}: ${value}`); // cucumber: 500 etc
})

// Array of [key, value] Pairs
let map2 = new Map([
['1',  'str1'],
[1,    'num1'],
[true, 'bool1']
]);

console.log( map2.get('1') ); // str1

// Create a map from an object
let obj2 = {
    name: 'John',
    age: 40
}
let map3 = new Map(Object.entries(obj2));
console.log(map3);
console.log(map3.get('name')); // John

// We can also create an object from Map 
let prices = Object.fromEntries([
    ['banana', 1],
    ['orange', 2],
    ['meat', 4]
])
console.log(prices);
console.log(prices.orange);

// Again, Map to Object
let map4 = new Map();
map4.set('banana', 1);
map4.set('orange', 2);
map4.set('meat', 4);
let obj4 = Object.fromEntries(map4.entries());
console.log(obj4);
console.log(obj4.orange);

// We could also do this
let obj5 = Object.fromEntries(map4);
console.log(obj5);

