/** 
 *  A Set is a special collection type "set of values" without keys where each may occur only once
 */

// Simple set
let numSet = new Set();
let one = 1;
let two = 2;
let three = 3;

numSet.add(one);
numSet.add(two);
numSet.add(three);
// numSet.add(one); <-- throws no error, but is ignored

console.log(numSet);
console.log(numSet.size);
console.log(numSet.values());

// Set with objects
let userSet = new Set();
let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

// Visits, some users come multiple times
userSet.add(john);
userSet.add(pete);
userSet.add(mary);
userSet.add(john); // <-- ignored
userSet.add(mary); // <-- ignored

// set keeps only unique values
console.log(userSet.size);

userSet.forEach(user => {  // Alternatively could have used "for (let user of set){ ... }"
    console.log(user.name);
});

console.log('\n');

// Advantages to using set over an array is that if you wanted to find unique values, you don't have to perform "array.find" to get the unique values.
// This gives you a dramatic performance advantage.

// Notice here we use three parameters in this forEach - this allows Map and Set to be interchangeable
let fruitSet = new Set(['oranges', 'apples', 'bananas']);
for (let item of fruitSet) console.log(item);

console.log('\n');

// The same with forEach
fruitSet.forEach((value, valueAgain, fruitSet) => {
    console.log('Value: ' + value);
    console.log('Value again: ' + valueAgain);
    console.log('Set: ' + fruitSet);
})

console.log('\n');

console.log('Set Keys');
console.log(fruitSet.keys());

console.log('Set Values');
console.log(fruitSet.values());

console.log('Set Entries');
console.log(fruitSet.entries());

console.log('\n');

// Let's do the same with Map
let fruitMap = new Map([
    [1, 'oranges'], 
    [2, 'apples'], 
    [3, 'bananas']
]);
for (let item of fruitMap) console.log(item);

console.log('\n');

console.log('Map Keys');
console.log(fruitMap.keys());

console.log('Map Values');
console.log(fruitMap.values());

console.log('Map Entries');
console.log(fruitMap.entries());

console.log('\n');
// The same with forEach
fruitMap.forEach((value, key, fruitMap) => {
    console.log('Value: ' + value);
    console.log('Key: ' + key);
    console.log('Set: ' + fruitMap);
})

// Map is a collection of keyed values
// Set is a collection of unique values 

/** 
 * Exercise - Filter to an array of unique values
 */
function unique(arr) {
// your code 
}

let values = ["Hare", "Krishna", "Hare", "Krishna",
"Krishna", "Krishna", "Hare", "Hare", ":-O"
];

console.log( unique(values) ); // Hare, Krishna, :-O

console.log('\n');

/**
 * Solution
 */
function filterUnique(arr) {
    return Array.from(new Set(arr));
}

console.log('Original Values');
console.log(values);

console.log('Unique Values');
console.log(filterUnique(values));

console.log('\n');
console.log('Anagrams output');

/** 
 *  Anagrams - words that have the same number of same letters, but in different order.
 */
let arr = ["nap", "teachers", "cheaters", "PAN", "ear", "era", "hectares"];

// Solution - returns an array cleaned from anagrams
function aclean(arrayParam) {
    let sortedWords = new Map();

    arrayParam.forEach(word => {
        let sortedWord = Array.from(word.toLowerCase()).sort().join('');
        sortedWords.set(word, sortedWord);
    })

    let cleanset = new Set();
    sortedWords.forEach((letters) => {
        cleanset.add(letters);
    })
    let uniqueWords = Array.from(cleanset);

    let cleaned = [];
    uniqueWords.forEach(word => {
        let entries = Array.from(sortedWords.entries());
        for (let entry of entries) {
            if (word === entry[1]) {
                cleaned.push(entry[0]);
                break;
            }
        }
    });
    return cleaned;
}

console.log(aclean(arr)); // "nap,teachers,ear" or "PAN,cheaters,era"

/**
 * Iterable Keys 
 */
let keyMap = new Map();
keyMap.set("name", "John");
let keys = keyMap.keys();

console.log('Iterable Keys');

// keys.push('more'); <- produced an error: keys.push is not a function, how do we solve this?

/* Solution */
keys = [...keyMap.keys()]; // can also do Array.from(keyMap.keys());
keys.push('more');
console.log(keys);