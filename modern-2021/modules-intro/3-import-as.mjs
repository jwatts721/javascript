import {sayHi as hi, sayBye as bye} from './1-export-apart-from-declaration.mjs';

hi('John'); // Hello, John!
bye('John'); // Bye, John!