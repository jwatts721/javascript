import * as say from './4-export-as.mjs';

say.hi('John'); // Hello, John!
say.bye('John'); // Bye, John!