import User from './5-export-default.mjs'; // not {User}, just User

let user = new User('Jack');

console.log(`Hello, ${user.name}!`);