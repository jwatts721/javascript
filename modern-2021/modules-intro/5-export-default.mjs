/**
 * Used for modules that declare a single entity and exports only that entity
 */
export default class User {
    constructor(name) {
        this._name = name;
    }
    get name() { return this._name; }
}