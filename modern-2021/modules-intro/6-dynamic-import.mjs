/**
 * Import expression
 */
import('./1-export-apart-from-declaration.mjs')
    .then((module) => {
        module.sayHi('Joe');
        module.sayBye('Joe');
    })
    .catch((error) => {
        console.log('No Such Module!');
    });


// Or dynamic import like this
let {sayHi, sayBye} = await import('./1-export-apart-from-declaration.mjs');

sayHi('Bob');
sayBye('Bob');


