===================================
Modules
===================================
Reference; https://javascript.info/import-export

Libraries to load modules on demand:
- AMD - one of the most ancient module systems, initially implemented by the library require.js (https://requirejs.org)
- CommonJS - the module system created for Node.js server
- UMD - one more module system, suggested as a universal one, compatible with AMD and CommonJS

What is a module?
A module is just a file. One script is one module. Simple as that.

Core module features:
- Modules always work in "use strict" mode.
- Each module has its own top-level scope. Variables from one module can't be see in other modules, even if they have the same name.
- Modules should "export" what they want to be accessible from outside ande import what they need from outside.
- Modules can export anything:
    - variables
    - functions
    - classes
See "main.js".

- The object import.meta contains the information about the current module.
    <script type="module">
    alert(import.meta.url); // script URL
    // for an inline script - the URL of the current HTML-page
    </script>

- In a module, "this" is undefined.

    <script>
    alert(this); // window
    </script>

    <script type="module">
    alert(this); // undefined
    </script>

-----------------------------
Browser-specific features:
-----------------------------
- Modules scripts are deferred
- Async works on inline scripts
    <!-- all dependencies are fetched (analytics.js), and the script runs -->
    <!-- doesn't wait for the document or other <script> tags -->
    <script async type="module">
        import {counter} from './analytics.js';

        counter.count();
    </script>

- No "bare" modules allowed. In the browser, import must get either a relative or absolute URL.
    import {sayHi} from 'sayHi'; <-- incorrect for browser, may work in NodeJS
    import {sayHi} from 'http://some-url/sayHi.js'; <-- correct behavior

- Compability, "nomodule"
    <script type="module">
        alert("Runs in modern browsers");
    </script>

    <script nomodule>
        alert("Modern browsers know both type=module and nomodule, so skip this")
        alert("Old browsers ignore script with unknown type=module, but execute this.");
    </script>

- Build Tools - in practice, browser modules are rarely use in their "raw" form. They are usually bundled
                with tools such as WebPack.


----------------------------------------------------------------
Export
----------------------------------------------------------------
Export before declarations:
    // export an array
    export let months = ['Jan', 'Feb', 'Mar','Apr', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    // export a constant
    export const MODULES_BECAME_STANDARD_YEAR = 2015;

    // export a class
    export class User {
    constructor(name) {
        this.name = name;
    }
    }

Export apart from declarations:
We can put export seperate from declarations
    // 📁 say.js
    function sayHi(user) {
    alert(`Hello, ${user}!`);
    }

    function sayBye(user) {
    alert(`Bye, ${user}!`);
    }

    export {sayHi, sayBye}; // a list of exported variables, functions
    // we can put export above the functions as well

----------------------------------------------------------------
Import
----------------------------------------------------------------
Usually we put what we want to import inside of curly braces {...}
    // 📁 main.js
    import {sayHi, sayBye} from './say.js';

    sayHi('John'); // Hello, John!
    sayBye('John'); // Bye, John!

However, we can handle many imports with a wildcard character '*'
    // 📁 main.js
    import * as say from './say.js';

    say.sayHi('John');
    say.sayBye('John');

But you should consider 3rd party builders that optimize code leaving out unused functions.
So be sure when using wildecard that you are using all of the functions. 

Import "as":
We can use "as" to import under different name:
    // 📁 main.js
    import {sayHi as hi, sayBye as bye} from './say.js';

    hi('John'); // Hello, John!
    bye('John'); // Bye, John!

Export "as". Same as above, just for export:
    // 📁 say.js
    ...
    export {sayHi as hi, sayBye as bye};

    // 📁 main.js
    import * as say from './say.js';

    say.hi('John'); // Hello, John!
    say.bye('John'); // Bye, John!


See additional examples in JS.

-----------------------------
Node.js
-----------------------------
- For Node.js we use the "*.mjs" extension to indicate to node that the JavaScript code is inside a module.

Dynamic imports:
-- see https://javascript.info/modules-dynamic-imports

In HTML, does not require the script type="module".