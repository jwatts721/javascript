export class UserDetail {
    constructor(user, age) {
        this.user = user;
        this.age = age;
    }
    get user() { return this._user; }
    set user(value) { this._user = value; }
    get age() { return this._age; }
    set age(value) { this._age = value; }
}