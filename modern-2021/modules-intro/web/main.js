import {user} from '/variable.js';
document.getElementById('user').innerHTML = user;

import {greeting} from '/function.js';
document.getElementById('greeting').innerHTML = greeting(user);

import { UserDetail } from './class.js';
const userDetail = new UserDetail(user, 37);
document.getElementById('user-details').innerHTML = userDetail.user + ' - age ' + userDetail.age;